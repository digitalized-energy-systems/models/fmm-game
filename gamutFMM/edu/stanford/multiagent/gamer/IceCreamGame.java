package edu.stanford.multiagent.gamer;

import java.io.*;
import java.util.*;
import java.nio.IntBuffer;

import edu.stanford.multiagent.gamer.Parameters.ParamInfo;
import edu.stanford.multiagent.gamer.graphs.*;

/*
 * Generate the Ice Cream Vendor game represented as an AGG
 * 
 */
 
public class IceCreamGame extends ActionGraphGame
{
	private static final int DISCONNECTION_ADJUSTMENT = 4;

	boolean randomize;

/* Parameters
 *
 */
	private static Parameters.ParamInfo pNumPlayers;
	private static Parameters.ParamInfo pCVendors;
	private static Parameters.ParamInfo pVVendors;
	private static Parameters.ParamInfo pWVendors;
	//private static Parameters.ParamInfo pLocations;
	
	private static Parameters.ParamInfo pNeighborWeight;
	private static Parameters.ParamInfo pFlavorWeight;
	
	private static Parameters.ParamInfo pDisconnect;
//	private double probDisconnect;
	
	private static Parameters.ParamInfo[] iceParam;

	//private static final int S = 8; //number of actions

	//private static final double neighborWeight= 0.6;//weight for effect from neighbor locations
	//private static final double flavorWeight=0.8; //weight for effect from different flavor
	
	static
	{
		pNumPlayers = new Parameters.ParamInfo("players", Parameters.ParamInfo.LONG_PARAM, new Long(2), new Long(100), "number of players (if set then different vendors will be randomized)", false);
		
		pCVendors = new Parameters.ParamInfo("c_vendors", Parameters.ParamInfo.LONG_PARAM,new Long(1), new Long(100),"number of chocolate vendors. between 1 and 100",false);
		pVVendors = new Parameters.ParamInfo("v_vendors", Parameters.ParamInfo.LONG_PARAM,new Long(1), new Long(100),"number of vanilla vendors. between 1 and 100",false);
		pWVendors = new Parameters.ParamInfo("w_vendors", Parameters.ParamInfo.LONG_PARAM,new Long(1), new Long(100),"number of west-side-only vendors. between 1 and 100",false);
		//pLocations = new Parameters.ParamInfo("locations", Parameters.ParamInfo.LONG_PARAM, new Long(2), new Long(100), "number of locations. between 2 and 100", true);

		pNeighborWeight = new Parameters.ParamInfo("neighbor_weight", Parameters.ParamInfo.DOUBLE_PARAM,new Double(0), new Double(1),"Weight [0,1] of effect between neighbors",false);
		pFlavorWeight = new Parameters.ParamInfo("flavor_weight", Parameters.ParamInfo.DOUBLE_PARAM,new Double(0), new Double(1),"Weight [0,1] of effect between flavors",false);
		pDisconnect = new Parameters.ParamInfo("disconnection_chance", Parameters.ParamInfo.DOUBLE_PARAM,new Double(0), new Double(1),"Chance [0,1] of a road between two neighbors to be disconnected",false);

		iceParam = new Parameters.ParamInfo[] { pNumPlayers, Game.symActions, pCVendors,pVVendors, pWVendors, pNeighborWeight, pFlavorWeight, pDisconnect};
		Global.registerParams(IceCreamGame.class, iceParam);
	}


	public IceCreamGame() throws Exception
	{
		super();
	}



	public void initialize() throws Exception
	{
		super.initialize();
		
		int c,v,w = 0;
		if (parameters.isParamSet(pNumPlayers.name)) {
			long numPlayers = parameters.getLongParameter(pNumPlayers.name);
			System.out.println("doGenerate: players parameter " + numPlayers + " overriding vendor types");
			c = (int)Global.randomLong( 1, numPlayers/2 );
			v = (int)Global.randomLong( 1, numPlayers - c -1 );
			w = (int)numPlayers - c - v;
			try {
				parameters.setParameter(pCVendors.name, (long)c);
				parameters.setParameter(pVVendors.name, (long) v);
				parameters.setParameter(pWVendors.name, (long)w);
			} catch (Exception e) {
				Global.handleError("error setting vendors" );
			}
		}

			if (!parameters.isParamSet(pCVendors.name))
				Global.handleError("Required parameter missing: c_vendors");
			if (!parameters.isParamSet(pVVendors.name))
				Global.handleError("Required parameter missing: v_vendors");
			if (!parameters.isParamSet(pWVendors.name))
				Global.handleError("Required parameter missing: w_vendors");

			c = (int)parameters.getLongParameter(pCVendors.name);
		v = (int)parameters.getLongParameter(pVVendors.name);
		w = (int)parameters.getLongParameter(pWVendors.name);
		
//		if (!parameters.isParamSet(pLocations.name))
//	    Global.handleError("RequirLed parameter missing: locations");
		
		//int  locations= (int) parameters.getLongParameter(pLocations.name);
		setNumPlayers(c+v+w);

		numActionNodes = Integer.valueOf(parameters.getVectorParameter(Game.symActions.name).get(0).toString());// locations * 2;

		numPNodes=0;
		int clocations = numActionNodes /2;
		int vlocations = numActionNodes - clocations;
		
		int[] CNodes =new int[clocations];
		for(int i=0;i<clocations;i++) CNodes[i]=i; 
		int[] VNodes =new int[vlocations];
		for(int i=0;i<vlocations;i++) VNodes[i] = i+clocations; 
		int[] WNodes= new int[(clocations/2)*2];
		for (int i = 0; i < clocations / 2; i++)
		{
		    WNodes[i] = i;
		    WNodes[i + clocations / 2] = i + clocations; //eg {0,1,4,5} for locations=4;
		}

		int[] a = new int[getNumPlayers()];
		actionSets= new int [getNumPlayers()][];
		for (int i = 0; i < c; i++)
		{
			actionSets[i] = (int[])CNodes.clone();
			a[i] = CNodes.length;
		}
		for (int i = c; i < c + v; i++)
		{
			actionSets[i] = (int[])VNodes.clone();
			a[i] = VNodes.length;
		}
		for (int i = c + v; i < c + v + w; i++)
		{
			actionSets[i] = (int[])WNodes.clone();
			a[i] = WNodes.length;
		}
		setNumActions(a); // number of actions 

		initGraph();
	}

	protected void initGraph()
	{
		try
		{
			graph = new IceCreamGraph();
			graph.setParameter("num_actions", new Long(numActionNodes), true);
			double probDisconnect = parameters.getDoubleParameter(pDisconnect.name);
			graph.setParameter("disconnection_chance_graph", probDisconnect, parameters.setByUser(pDisconnect.name));
			graph.initialize();
		}
		catch (Exception e)
		{
			Global.handleError(e, "initializing graph");
		}
	}
	protected void checkParameters() throws Exception
	{
		if(!parameters.setByUser(pNumPlayers.name) && 
				!(parameters.setByUser(pCVendors.name) &&
						parameters.setByUser(pVVendors.name)&&
						parameters.setByUser(pWVendors.name))){
			
			throw new IllegalArgumentException("either players or vendors must be set");
		}
	}

	public void setParameters(ParamParser p, boolean randomize) 
	throws Exception
	{
		super.setParameters(p, randomize);
		this.randomize = randomize;
	}
	public void randomizeParameters()
	{
		try{
			if(!parameters.setByUser(pNeighborWeight.name))
			    parameters.setParameter(pNeighborWeight.name, 
						    Global.rand.nextDouble());
			if(!parameters.setByUser(pFlavorWeight.name))
			    parameters.setParameter(pFlavorWeight.name, 
						    Global.rand.nextDouble());
			if(!parameters.setByUser(pDisconnect.name)){
				double disconnectChance = Global.rand.nextDouble()/DISCONNECTION_ADJUSTMENT;
//				probDisconnect = disconnectChance;
				parameters.setParameter(pDisconnect.name, disconnectChance, true);
			}
		}
		catch (Exception e)
		{
			Global.handleError(e, "Randomizing");
		}

		super.randomizeParameters();
	}


	public void doGenerate()
	{
		setDescription("Ice Cream Vendor Game represented as AGG\n" + getDescription());
		setName("Ice Cream Game");
		
		
		graph.doGenerate();

		double neighborWeight = parameters.getDoubleParameter(pNeighborWeight.name);
		double flavorWeight = parameters.getDoubleParameter(pFlavorWeight.name);
		
		//generate payoffs
		TreeSet[] configs = initConfigs();
		for (int node = 0; node < numActionNodes; node++)  //for each action node
		{
			TreeMap<IntBuffer, Double> payoffMap = new TreeMap<IntBuffer, Double>();
			graph.setNodeData(node, payoffMap);

			int k = graph.getNumNeighbours(node);

			
			Iterator confIter = configs[node].iterator();
			while (confIter.hasNext())
			{
				IntBuffer tt= (IntBuffer)confIter.next();
				int[] key = (int[])tt.array();
				/**utility: sum of number of  vendors of different flavor in the neighborhood minus the sum of 
				 * number of vendors of the same flavor in the neighborhood
				 */
				double u = 0.0;
				Iterator edgeIter = graph.getEdges(node);
				for (int idx = 0; edgeIter.hasNext(); idx++) //for each neighbor of the current action node
				{

					Edge e = (Edge)edgeIter.next();
					int nei = e.getDest();
					if ((node < numActionNodes / 2 && nei < numActionNodes / 2) || (node >= numActionNodes / 2 && nei >= numActionNodes / 2)) //if same flavor
					{
						if (node==nei)u -= key[idx];
						else u -= neighborWeight*key[idx];
					}
					else
					{
						if(node==nei+numActionNodes/2 ||nei==node+numActionNodes/2)
							u += flavorWeight* key[idx];
						else
							u+= flavorWeight * neighborWeight*key[idx];
					}
				}
				payoffMap.put(IntBuffer.wrap((int[])key.clone()), new Double(u));
			}

		}


	}

	protected String getGameHelp()
	{
		return "Generate an Ice Cream game.\n";
	}


	private static class IceCreamGraph extends ALGraph
	{
		protected static Parameters.ParamInfo pActions;
		protected static Parameters.ParamInfo pDisconnect;
		protected static Parameters.ParamInfo[] icgParam;
		 
		static
		{
			pDisconnect = new Parameters.ParamInfo("disconnection_chance_graph", Parameters.ParamInfo.DOUBLE_PARAM, new Double(0), new Double(1), "chance of a road being disconnected", true);
			pActions = new Parameters.ParamInfo("num_actions", Parameters.ParamInfo.LONG_PARAM, new Long(2), new Long(100), "number of actions. between 2 and 100", true);
			icgParam = new Parameters.ParamInfo[] {pActions, pDisconnect };
			Global.registerParams(IceCreamGraph.class, icgParam);
		}

		protected String getGraphHelp()
		{
			return "Action graph for the Ice Cream Vendor game.";
		}
		IceCreamGraph() throws Exception
		{
			super();
		}
		public void initialize() throws Exception
		{
			super.initialize();
			if (!parameters.isParamSet(pDisconnect.name))
			    Global.handleError("Required parameter missing: disconnection_chance");
				
			if (!parameters.isParamSet(pActions.name))
			    Global.handleError("Required parameter missing: actions");
			int numActions = (int)parameters.getLongParameter(pActions.name);
			
			if (nodes.isEmpty())
			{
				
				for (int i = 0; i < numActions; i++) addNode();
			}
		}
		public boolean reflexEdgesOk()
		{
			return (true);
		}
		public boolean hasSymEdges()
		{
			return (true);
		}
		protected void checkParameters() throws Exception
		{
		}
		public void doGenerate()
		{
			int S = (int)parameters.getLongParameter(pActions.name);
			double disconnectionChance = parameters.getDoubleParameter(pDisconnect.name);
			
			//self edges
			for (int i = 0; i < S; i++) addEdge(i, i);

			int cLocs = S/2;
			int lastNode = S-1;
			if( S%2 != 0 ){ // there is one more vLoc than cLoc
				if(Global.randomDouble() < disconnectionChance)
					addEdge(lastNode,lastNode-1);
				if(Global.randomDouble() < disconnectionChance)
					addEdge(lastNode,cLocs-1);
				if(Global.randomDouble() < disconnectionChance)
					addEdge(lastNode-1,lastNode);
				if(Global.randomDouble() < disconnectionChance)
					addEdge(cLocs-1, lastNode);
			}
			for (int i = 0; i <cLocs; i++)
			{
				//edge between the two flavors at each location:
					addEdge(i, i + cLocs);
					addEdge(i + cLocs, i);

				//edge between neighboring locations
				if (i > 0)
				{
					if(Global.randomDouble() < disconnectionChance)
					addEdge(i, i - 1);
					if(Global.randomDouble() < disconnectionChance)
					addEdge(i + cLocs, i + cLocs - 1);
					if(Global.randomDouble() < disconnectionChance)
					addEdge(i, i + cLocs - 1);
					if(Global.randomDouble() < disconnectionChance)
					addEdge(i + cLocs, i - 1);
				}
				if (i < cLocs - 1)
				{
					if(Global.randomDouble() < disconnectionChance)
					addEdge(i, i + 1);
					if(Global.randomDouble() < disconnectionChance)
					addEdge(i + cLocs, i + cLocs + 1);
					if(Global.randomDouble() < disconnectionChance)
					addEdge(i, i + cLocs + 1);
					if(Global.randomDouble() < disconnectionChance)
					addEdge(i + cLocs, i + 1);
				}
			}
		}

	}
}