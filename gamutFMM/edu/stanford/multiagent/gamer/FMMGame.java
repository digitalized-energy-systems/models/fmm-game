package edu.stanford.multiagent.gamer;

import java.util.*;
import java.util.Map.Entry;
import java.nio.IntBuffer;

import edu.stanford.multiagent.gamer.graphs.*; 

/**
 * @author Gesa Ohlendorf
 * Generate the FMM game represented as an AGG
 */
public class FMMGame extends ActionGraphGame implements FMMGameInterface {

	private static class ActionNode
    {
		double[] bid_amounts;
    	int[] bid_prices;
    	int[] MW_cost;
    	int[] MWh_cost;
    	
    	public ActionNode(double[] bid_amounts, int[] bid_prices, int[] MW_cost, int[] MWh_cost)
    	{
    		this.bid_amounts = bid_amounts;
        	this.bid_prices=bid_prices;
        	this.MW_cost = MW_cost;
        	this.MWh_cost = MWh_cost;
    	}
    	
    	/**
    	 * Checks whether two ANs represent the same
    	 * action but with different utility functions
    	 * @param that AN to compare this to
    	 * @return true/false
    	 */
		public boolean sameAction(ActionNode that) {
			
	        if(Arrays.equals(bid_amounts,that.bid_amounts) &&
	           Arrays.equals(this.bid_prices, that.bid_prices))
	        	return true;
	        else
	        	return false;
		}

		@Override
    	/**
    	 * ActionNode objects are considered equal if they have the same values for all of their properties
    	 * (override so that equal doesn't require the memory address to be the same,
    	 *  that would otherwise only be the case for a comparison of an ActionNode object with itself)
    	 */
	    public boolean equals(Object o) {
			if (o == this) return true;
	        if (!(o instanceof ActionNode)) return false;
	        return(
	 	         Arrays.equals(bid_amounts,((ActionNode)o).bid_amounts) &&
		         Arrays.equals(this.bid_prices,((ActionNode)o).bid_prices) &&
		         Arrays.equals(MW_cost,((ActionNode)o).MW_cost) &&
		         Arrays.equals(MWh_cost,((ActionNode)o).MWh_cost));
	    }
    	
    	@Override
    	/**
    	 * objects must be equal if and only if their hashcodes are equal
    	 */
        public int hashCode() {
    		return Arrays.hashCode(bid_amounts)+Arrays.hashCode(bid_prices)+Arrays.hashCode(MW_cost)+Arrays.hashCode(MWh_cost);
        }
		
    }
	
private static class Playerclass extends ParameterizedObject {
		
		int numPlayers;
		int[] MW_cost;
		int[] MWh_cost;
		String[][] actions;
				
		protected static Parameters.ParamInfo p_numPlayers;
		protected static Parameters.ParamInfo p_MWcost;
		protected static Parameters.ParamInfo p_MWhcost;
		protected static Parameters.ParamInfo p_Actions;
		protected static Parameters.ParamInfo[] pcParam;
		
		static
		{
			p_numPlayers= new Parameters.ParamInfo("num_players", Parameters.ParamInfo.LONG_PARAM,new Long(1),Long.MAX_VALUE,"number of players per playerclass, at least 1",true);
			p_MWcost = new Parameters.ParamInfo("MW_cost", Parameters.ParamInfo.STRING_PARAM,Long.MIN_VALUE,Long.MAX_VALUE,"standby cost for every MW per hour in cent,  1 integer per time slot dividided by comma, must be enclosed in [] ",true);
			p_MWhcost = new Parameters.ParamInfo("MWh_cost", Parameters.ParamInfo.STRING_PARAM,Long.MIN_VALUE,Long.MAX_VALUE,"provision cost for every called MW for every hour in cent, 1 integer per time slot dividided by comma, must be enclosed in [] ",true);
			p_Actions = new Parameters.ParamInfo("actions", Parameters.ParamInfo.VECTOR_PARAM, null, null, "Vector with all possible actions for this playerclass' type, must be enclosed in []",true);
			
			pcParam = new Parameters.ParamInfo[] {p_numPlayers,  p_MWcost, p_MWhcost, p_Actions};
			Global.registerParams(Playerclass.class, pcParam);
		}
		
		 /**
	     * The constructor.  Must be called by all subclasses.
	     * @ pcKind kind of playerclass (RLM, FMM)
	     * @throws Exception
	     */
		protected Playerclass() throws Exception {
			super();
		}
		
		/**
	     * Initializes Playerclass using preset parameter values
	     *
	     * @throws Exception
	     */
	    public void initialize() throws Exception
	    {
	    	super.initialize(); 
	    	
	    	// Get parameters
			this.numPlayers = (int)parameters.getLongParameter(p_numPlayers.name);
			String mwString = parameters.getStringParameter(p_MWcost.name);
			this.MW_cost = Arrays.stream(mwString.split(",")).map(String::trim).mapToInt(Integer::parseInt).toArray();
			String mwhString = parameters.getStringParameter(p_MWhcost.name);
			this.MWh_cost = Arrays.stream(mwhString.split(",")).map(String::trim).mapToInt(Integer::parseInt).toArray();
			Vector<String> actionsVec = parameters.getVectorParameter(p_Actions.name);
			this.actions = new String[actionsVec.size()][];
			int i = 0;
			for(String a : actionsVec) {
				this.actions[i] = a.split(",");
				i++;
			}			
	    }

		@Override
		/**
		 * Check that type distributions adds up to 1
		 */
		protected void checkParameters() throws Exception {
			
		}

		@Override
		public void doGenerate() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public String getHelp() {
			// TODO Auto-generated method stub
			return null;
		}	
	}
	
	private boolean noBidused=false;
	private int num_tSlots;
	private int UP;
	private double pCall;
	private HashMap<Integer, Playerclass> pcs = new HashMap<Integer, Playerclass>();
	private Vector<ActionNode> ans = new Vector<ActionNode>();
	private HashMap<Integer, Vector<Integer>>[] bprices;
	
	/* Parameters
	 *
	 */	
	private static Parameters.ParamInfo p_pCall;
	private static Parameters.ParamInfo p_numtSlots;
	private static Parameters.ParamInfo playerClassParams;
	private static Parameters.ParamInfo p_upRLM;
	private static Parameters.ParamInfo[] fmmParam;
	
	static
	{
		p_pCall = new Parameters.ParamInfo("p_call", Parameters.ParamInfo.DOUBLE_PARAM,new Double(0), new Double(1),"probability in [0,1] that accepted tenders have to be provided over the full time span and bid amount ",true);
		p_numtSlots= new Parameters.ParamInfo("num_tSlots", Parameters.ParamInfo.LONG_PARAM,Long.MIN_VALUE,Long.MAX_VALUE,"number of timeslots per product time slice",true);
		playerClassParams = new Parameters.ParamInfo("playerclasses", Parameters.ParamInfo.NESTEDHASHMAP_PARAM, null, null, "parameters per playerclass, must be enclosed in [].",true);
		p_upRLM = new Parameters.ParamInfo("up_rlm", Parameters.ParamInfo.LONG_PARAM,Long.MIN_VALUE,Long.MAX_VALUE,"uniform price at central balancing capacity market in cent",true);
		
		fmmParam = new Parameters.ParamInfo[] {p_pCall, p_numtSlots, playerClassParams, p_upRLM};
		Global.registerParams(FMMGame.class, fmmParam);
	}
	
	/**
	 * /**
	 * 
	 * @param index index of first player
	 * @param pcVec playerclass parameters
	 * @param market kind of playerclass (RLM, FMM)
	 * @return index of last player+1
	 * @return
	 * @throws Exception 
	 */
	private int addPClass(int index, Vector<HashMap> pcVec) throws Exception {
    	   	
    	Playerclass pc = new Playerclass();
    	ParamParser p=null;

    	try {
    		if(pcVec.size()!=1)
    			throw new Exception("Error reading in playerclass parameters: unexpected number of parameters in "+pcVec);
    	    p = new ParamParser(pcVec.firstElement()); 
    	    pc.setParameters(p, false);

    	} catch (Exception e) {
    	    System.err.println(e.toString());
    	    System.err.println(Global.getHelp());
    	    System.exit(1);
    	}
    	
    	pc.initialize();    	
    	pcs.put(index, pc);

    	return index+pc.numPlayers;
    }

	public FMMGame() throws Exception {
		super();
	}
	
	public void initialize() throws Exception
	{
		super.initialize(); 
		
		// Get parameters
		num_tSlots = (int)parameters.getLongParameter(p_numtSlots.name);
		UP = (int)parameters.getLongParameter(p_upRLM.name);			// given in cent per MW PER HOUR
		pCall = parameters.getDoubleParameter(p_pCall.name);
		Vector<Vector> pclasses = (Vector)parameters.getVectorParameter(playerClassParams.name).get(0);
		
		// Initialize ActionNodes 
		int[] zero = {0};
		double[] dzero = {0.0};
		final ActionNode an = new ActionNode(dzero,zero,zero,zero); 
		ans.add(an);
		
		// Initialize PlayerClasses
		int numPlayers = 0;
		for(Vector pclass : pclasses) {
			numPlayers = addPClass(numPlayers, pclass);
		}
	
		/** Evaluate parameters to set (AG)Game values */
		actionSets = new int[numPlayers][]; // init AN indices per player over all types
		int p = 0;
		// for each PlayerClass
		for(Playerclass PC : pcs.values()) {
			
			int numActions = PC.actions.length;
			int[] MW_cost = PC.MW_cost;
			if(MW_cost.length != num_tSlots)
				Global.handleError("Unexpected Number of elements in MW_cost: " + Arrays.toString(MW_cost));
			int[] MWh_cost = PC.MWh_cost;
			if(MWh_cost.length != num_tSlots)
				Global.handleError("Unexpected Number of elements in MWh_cost: " + Arrays.toString(MWh_cost));
			
			// for each player in PC
			for(int i=0; i<PC.numPlayers; i++) {
				actionSets[p+i] = new int[numActions]; 
			}
				
			// for each action
			int a = 0;
			for(String[] action : PC.actions) {
				
				// Check that number of given bid amounts and prices is correct
				if(action.length != 2*num_tSlots)
					Global.handleError("Unexpected Number of elements in action: " + Arrays.toString(action));
				
				// check for "no bid" (whether all bid amounts are 0)
			    boolean bool_bid = false;
			    for(int m=0; m<action.length; m=m+2) {
			    	try {
				    	if(Integer.valueOf(action[m])!=0) {
				    		bool_bid = true;
				    		break;
				    	}
			    	} catch (Exception e) {
		    			Global.handleError(e, "Error reading in bid amount from action "+Arrays.toString(action)+"! Bid amounts and prices have to be Integer values!");
		    		}
			    }
			    // case no bid
			    if(!bool_bid) {
			    	noBidused=true;		    	
			    	// for each player in PC add "no bid"-AN 0 to type-action set
					for(int i=0; i<PC.numPlayers; i++) 
						actionSets[p+i][a] = 0; 
			    }			    	
			    else {
			    	// Get bid amounts and prices
			    	double[] bid_amounts = new double[action.length/2];
			    	int[] bid_prices = new int[action.length/2];
			    	for(int m=0; m<action.length/2; m++) {
			    		try {
				    		bid_amounts[m] = (double)Integer.valueOf(action[2*m]); // make sure bid amounts are given as integers, only handle as double internally
				    		if(bid_amounts[m]==0) // no bid for this time slot, so bid price doesn't matter
					    		bid_prices[m]=0;
					    	else
					    		bid_prices[m] = Integer.valueOf(action[2*m+1]);
			    		} catch (Exception e) {
			    			Global.handleError(e, "Error reading in bid amount from action "+Arrays.toString(action)+"! Bid amounts and prices have to be Integer values!");
			    		}
				    }
			    	// Check whether a corresponding AN is already listed
			    	ActionNode thisAN = new ActionNode(bid_amounts,bid_prices,MW_cost,MWh_cost);
			    	int ANindex = 0;
			    	// Reuse AN
			    	if(ans.contains(thisAN)) {
			    		Vector<Integer> ANindices = Global.getIndicesFromValue(ans, thisAN);
			    		if(ANindices.size()!=1)
			    			Global.handleError("Error setting ANs with indices: " + ANindices);
			    		ANindex = ANindices.get(0);
			    	}
			    	// Add AN to ans
			    	else {
			    		ANindex = ans.size();
			    		ans.add(thisAN);
			    	}
			    	// Update action set for each player of this PC: 
			    	// add this bid's AN (that is index ANindex) to type-action set
		    		for(int i=0; i<PC.numPlayers; i++) 
						actionSets[p+i][a] = ANindex;
			    }
			    a++; // increment action index
			}
			// Make sure AN indices are saved in ascending order
			for(int i=0; i<PC.numPlayers; i++) 
				Arrays.sort(actionSets[p+i]);

			// update player index
			p += PC.numPlayers;
		}
		
		// In case there's no NO BID (no AN 0)
		if(!noBidused) {
			
			ans.remove(0);
			
			for(int i=0; i<numPlayers; i++) {
				for(int j=0; j<actionSets[i].length; j++)
					actionSets[i][j] = actionSets[i][j]-1;
			}
		}
		
		/** Set (AG)Game values */
		setNumPlayers(numPlayers);
		// set numANs
		numActionNodes = ans.size();
		// set actionSets
		/* see above: actionSets[][] */
		// set setNumActions
		int[] a = new int[numPlayers];
		for (int i=0;i<a.length; i++) a[i] = actionSets[i].length;
		setNumActions(a);
		// set numFNs
		numPNodes = num_tSlots; // init: 1 overall sum FN per timeslot
		bprices = new HashMap[num_tSlots];				// one HashMap as entry per timeslot
		for(int slot=0; slot<num_tSlots; slot++) {		// Get all bid prices per timeslot
			
			HashMap<Integer, Vector<Integer>> priceMap = new HashMap<Integer, Vector<Integer>>();			// key: bid price (<=UP), value: Vector vprice with all ANs with this bid price
			
			for(int node=0; node<ans.size(); node++) {			

				if(this.noBidused && node==0) // Not for no bid AN (k=0), first look at FMM-ANs only
					continue;
				
				Vector<Integer> vprice = new Vector<Integer>();
				
				ActionNode thisAN = ans.get(node);
				int price = thisAN.bid_prices[slot];
				
				if((price==0) && (thisAN.bid_amounts[slot]==0))	// no bid for this timeslot
					continue;
				if (price>UP)
					continue;
				if(priceMap.containsKey(price)) 
					vprice = (Vector)priceMap.get(price);
				
				vprice.add(node);
				priceMap.put(price, vprice);
			}
			bprices[slot] = priceMap;
			
			// Add PNodes
	        numPNodes += priceMap.size();					// FNs to aggregate all FMM-bids of the same price (but for no bids (and prices>UP if !RLM))
			numPNodes += Global.max(0,priceMap.size()-2);	// FNs per price to aggregate all more expensive (cheaper) ANs (but for the most and second-most expensive (cheapest) per timeslot)
		}
		
		initGraph();
	}
	
	protected void initGraph() // TODO
	{
		try
		{
			graph = new FMMGraph();
			graph.setParameter("num_tSlots", new Long(num_tSlots), true);
			graph.setParameter("up_rlm", new Long(UP), true);
			graph.setParameter("num_actions", new Long(numActionNodes), true);
			graph.setParameter("num_functions", new Long(numPNodes), true);
			graph.setParameter("bid_prices", bprices, true);
			graph.setParameter("anodes_vec", ans, true);
			
			graph.initialize();

		}
		catch (Exception e)
		{
			System.err.println(getHelp());
			System.err.println(graph.getHelp());
			e.printStackTrace();
			Global.handleError(e, "initializing graph");
		}
	}
	
	/**
	 * Calculates the surplus of aggregated bids for one timeslot with
	 * bid prices <=UP that will not be accepted either because the
	 * slot amount cannot be aggregated over all timeslots or because
	 * it has to be rounded down to full MW (in general both)
	 * 
	 * @param overallsum: minimum of aggregated bid amounts over all
	 * timeslots rounded down to full MW (multiple of 1000kW)
	 * @param slotsum: aggregated bid amount over a timeslot, NOT rounded down
	 * @return surplus of aggregated bids for this timeslot compared to
	 * aggregated bids over all timeslots
	 * @throws Exception
	 */
	private int getSurplus(int overallsum, int slotsum) throws Exception{
		if(overallsum%MW >0)
			throw new Exception("Error determining surplus: Overallsum is not rounded down to MW.");
		if(overallsum>slotsum)
			throw new Exception("Error determining surplus: MW aggregated per slot is smaller than aggregated sum over all slots!");
		return (slotsum-overallsum);
	}
	
	
	
	@Override
	protected void checkParameters() throws Exception {
	}
	
	@Override 
	public void doGenerate() {
		
		setDescription("FMM Game represented as AGG\n" + getDescription());
		setName("FMM Game");
		
		// Generate the graph for the game.
		graph.doGenerate(); 
		
		/** Generate payoffs */
		TreeSet [] configs = initConfigs();	
		for (int node=0; node < numActionNodes; node++)	// for each AN
		{
			// NEW AN
			ActionNode thisAN = ans.get(node);
			
			TreeMap<IntBuffer, Double> payoffMap = new TreeMap<IntBuffer, Double>();
			graph.setNodeData(node, payoffMap);
			Iterator confIter = configs[node].iterator();
			while (confIter.hasNext()){		// for each config
							
				IntBuffer tt = (IntBuffer)confIter.next();	 
				int[] key = (int[])tt.array();	// key ist Array mit einer Config, z.B. [1,0,0,0]
								
				// Init value to save the...
				double u=0.0;								// ...utility for this AN and configuration
				int overallsum=0;							// ...overall aggregated bid amount (rounded down to full MW)
				Vector oSum = new Vector(); 				// ..."overall sum" FN configuration for every timeslot
				int tSlot = 0;								// ...current timeslot
				int numSlotFNs = 2*bprices[tSlot].size()-2;	// ...number of (more expensive & cheaper) FNs for the current timeslot
				int firstFN = numActionNodes+num_tSlots;	// ...index of the first FN for the current timeslot
				int surplus=0;								// ...surplus amount for the current timeslot
				boolean next_tSlot = false;

				Iterator edgeIter = graph.getEdges(node);				
				for (int idx = 0; edgeIter.hasNext(); idx++){	// idx is index to iterate over key-array from left to right (e.g. key[0]=1, key[1]=0, key[2]=0, key[3]=0)
				// NEW NEIGHBOR
					
					Edge e = (Edge)edgeIter.next();
					
					// calculate overallsum 
					if(idx<num_tSlots) {
						
						// update overallsum
						if(overallsum==0) overallsum = key[idx]-key[idx]%MW; 			// first time this is called
						else overallsum = Global.min(overallsum, key[idx]-key[idx]%MW);	// rounding aggregated kW down to MW (1000kW)
						
						// check that bids can be aggregated so far
						if(overallsum<=0) break; // no bids aggregated, utility is 0
						
						// save aggregated kW for this timeslot
						oSum.add(key[idx]); 
						continue;
					}
					
					// calculate surplus per slot
					if(idx==num_tSlots) { // 
						
						for(int slot=0; slot<num_tSlots; slot++) {
							int this_oSum = (int) oSum.elementAt(slot);
							try {
								surplus = getSurplus(overallsum, this_oSum);
							} catch (Exception error) { 
								Global.handleError(error, "Unable to generate the payoffs for FMMGraph");
							}
							oSum.setElementAt(surplus, slot); // oSum now contains surplus
						}
						surplus = (int) oSum.elementAt(tSlot); // init value
					}
									
					int nei = e.getDest(); 				// Index Neighbor
					
					if(nei<numActionNodes)
						Global.handleError("Error generating the payoffs for FMMGraph: AN has an AN neighbor!");
					
					while(nei>=firstFN+numSlotFNs || next_tSlot) {
						firstFN += numSlotFNs;
						tSlot++;
						next_tSlot = false;
						numSlotFNs = 2*bprices[tSlot].size()-2;
						surplus = (int) oSum.elementAt(tSlot);
						
					}
					
					if(nei<firstFN)	// don't need to evaluate this neighbor, e.g. because there was no surplus
						continue;	// or because more exp. FNs (at least) matched surplus amount, so same price FNs don't matter 
					
					double aa = thisAN.bid_amounts[tSlot];
					if(surplus==0) // no surplus for this tSlot
					{ 
						u += getUtilityperSlot(num_tSlots,aa,(double)UP,thisAN.MW_cost[tSlot],thisAN.MWh_cost[tSlot],pCall);
						
						if(tSlot==num_tSlots-1)	break;	// finished with last timeslot	
						next_tSlot = true;
						continue;
					}
					
					// more expensive FN
					if(!graph.areNeighbours(nei, node)) 
					{
						surplus -= key[idx];
						
						if(surplus<=0) // surplus has been matched by more exp. FNs
						{
							u += getUtilityperSlot(num_tSlots,aa,(double)UP,thisAN.MW_cost[tSlot],thisAN.MWh_cost[tSlot],pCall);
							
							if(tSlot==num_tSlots-1)	break; // finished with last timeslot
							next_tSlot = true;	// no need to look at same price FNs anymore
						}
						continue;
					}
					
					// same price FN (ELSE, if node is also neighbor of nei)
					surplus -= key[idx];
					if(surplus<0)	// tender made will be proportionally accepted
					{
						aa *= (double)Math.abs(surplus)/(double)key[idx]; //|surplus| is the part of the same price amount key[idx] that is accepted (the bid award is divided proportionally to the bid amount between all bids of the same price)
						u += getUtilityperSlot(num_tSlots,aa,(double)UP,thisAN.MW_cost[tSlot],thisAN.MWh_cost[tSlot],pCall);
					}
					
					if(tSlot==num_tSlots-1) break;	// finished with last timeslot
					next_tSlot = true;
					continue;

				}
				u = Global.round(ROUND_DIGITS,u);
				payoffMap.put(IntBuffer.wrap((int[])key.clone()), new Double(u));
			}
		}	
	}

	@Override
	protected String getGameHelp() {
		return "Generate a FMM game.\n";
	}

	// Graph modeling FMM only, uniform price at central RLM is given
	private static class FMMGraph extends ALGraph {
		
		protected static Parameters.ParamInfo pActions;
		protected static Parameters.ParamInfo pFunctions;
		protected static Parameters.ParamInfo p_numtSlots;
		protected static Parameters.ParamInfo pPrices;
		protected static Parameters.ParamInfo p_upRLM;
		protected static Parameters.ParamInfo pAnsVec;
		private static Parameters.ParamInfo[] fgParam;
		
		static
		{
			pActions = new Parameters.ParamInfo("num_actions", Parameters.ParamInfo.LONG_PARAM, new Long(1), Long.MAX_VALUE, "number of ANs. at least 1", true);
			pFunctions = new Parameters.ParamInfo("num_functions", Parameters.ParamInfo.LONG_PARAM, new Long(0), Long.MAX_VALUE, "number of FNs", true);
			p_numtSlots= new Parameters.ParamInfo("num_tSlots", Parameters.ParamInfo.LONG_PARAM,Long.MIN_VALUE,Long.MAX_VALUE,"number of timeslots per product time slice",true);
			pPrices = new Parameters.ParamInfo("bid_prices", Parameters.ParamInfo.HASHARRAY_PARAM, null, null, "Array containing a hashmap with all given bid prices per timeslot", true);
			p_upRLM = new Parameters.ParamInfo("up_rlm", Parameters.ParamInfo.LONG_PARAM,Long.MIN_VALUE,Long.MAX_VALUE,"uniform price at central capacity market in cent",false);
			pAnsVec = new Parameters.ParamInfo("anodes_vec", Parameters.ParamInfo.VECTOR_PARAM, null, null, "Vector with AN indices as indices and ActionNode objects as values", true);
			
			fgParam = new Parameters.ParamInfo[] {pActions, pFunctions, p_numtSlots, pPrices, p_upRLM, pAnsVec}; // pAmounts,
			Global.registerParams(FMMGraph.class, fgParam);
		}
	
		public FMMGraph() throws Exception {
			super();
		}
				
		public void initialize() throws Exception
		{
			super.initialize();
			
			int S = 0; // #ANs
			int P = 0; // #FNs
			
			try {
				S = (int)parameters.getLongParameter(pActions.name);
				P = (int)parameters.getLongParameter(pFunctions.name);
			} catch (Exception e) {
				Global.handleError(e, "Unable to get parameters to " +
						   "initialize FMMGraph");
		    }
			
			if (nodes.isEmpty()) {
				for (int i = 0; i < S; i++) addNode();
				for (int i=S; i<S+P; i++) addFNode(i, FunctionNode.EXTENDED_SUM, 0);
			}	
		}
		
		@Override
		protected String getGraphHelp() {
			return "Action graph for the FMM game.";
		}
		
		@Override
		public boolean hasSymEdges() {
			return false;
		}
		
		@Override
		public boolean reflexEdgesOk() {
			return (true);	
		}

		@Override
		/** Generate Action Graph with calls of function addEdge(end, start)
		 *  Attention: make sure neighbors are added in ascending order
		 *  of indices (for both ANs and FNs)
		 */
		public void doGenerate() {
			
			int S = 0; // #ANs
			int P = 0; // #FNs
			int num_tSlots = 0;
			int UP = 0;
			HashMap<Integer, Vector<Integer>>[] bpriceMapArray = new HashMap[0];
			Vector<ActionNode> ansVec = new Vector<ActionNode>();
		
			try {
				S = (int)parameters.getLongParameter(pActions.name);
				P = (int)parameters.getLongParameter(pFunctions.name);
				num_tSlots = (int)parameters.getLongParameter(p_numtSlots.name);
				UP = (int)parameters.getLongParameter(p_upRLM.name);
				bpriceMapArray = parameters.getHashMapArrayParameter(pPrices.name);
				ansVec = parameters.getVectorParameter(pAnsVec.name);
			} catch (Exception e) {
				Global.handleError(e, "Unable to get parameters to " +
						   "generate FMMGraph");
		    }
			
			/* Generate Action Graph with calls of function addEdge(end, start)
			 *  Attention: make sure neighbors are added in ascending order
			 *  of indices! (for both ANs and FNs)
			 */
			
			int FN_index = S+num_tSlots; // first FN index after overall sum FNs (1 overall sum FN per timeslot)
			int tSlot = 0;
			for (HashMap<Integer, Vector<Integer>> bpriceMap: bpriceMapArray) {	// run through once per timeslot
			// NEW TIMESLOT	
				
				int FN_start = FN_index; 								// first index per timeslot
				Vector<Integer> moreexp_ANs = new Vector<Integer>();	// vector for all "more expensive ANs" indices
				
				// Create descending Merit-Order-List
				Comparator<Integer> keyComparator = (s1, s2) -> s1.compareTo(s2);	// Create a Comparator, which provides logic to sort HashMap by key.
				TreeMap<Integer, Vector> treeMap = new TreeMap<>(keyComparator.reversed());	// Create a TreeMap using Comparator that imposes the reverse of the natural ordering of the objects.
				treeMap.putAll(bpriceMap);											// Add all the entries of bpriceMap into the TreeMap
				
				// Iterate over MOL to set edges
				Set<Entry<Integer, Vector>> MOLSet_DESC = treeMap.entrySet();
				for (Entry<Integer, Vector> entry : MOLSet_DESC) { // entry: Set sorted by price (key of HashMap bpriceMap)
				// NEW PRICE	
					
					if (entry.getKey() > UP) // check price <= UP 
						Global.handleError("Error generating FMMGraph: UP has changed somehow..."); // per definition above only bids with prices<=UP are contained in bpriceMapArray, so this error couldn't have happened..
					
					if((FN_index-FN_start)>1)	// neither the most expensive nor the second-most expensive price
					{
						// Edges from all more expensive ANs to "MORE EXPENSIVE" FN
						Collections.sort(moreexp_ANs); // make sure neighbors are saved in ascending order
						for (Integer moreexp_AN: moreexp_ANs) {
							addEdge(FN_index, moreexp_AN);
							// Set weight = bid amount of moreexp_AN (extended sum FN)
							try {
								FunctionNode fn = getFN(FN_index);
								int weight = (int)ansVec.get(moreexp_AN).bid_amounts[tSlot]; 
								fn.addWeight(moreexp_AN, weight);
							} catch (Exception error){
								Global.handleError(error, "Error setting weight for function node with index "+FN_index);
							}
						}
						FN_index++;
					}
					
					for (Object AN_index: entry.getValue()){	// iterate over all indices of this price
					// NEW AN	
						int this_AN = (Integer)AN_index;
						
						// Edge from "OVERALL SUM" FNs
						for(int i=0; i<num_tSlots; i++) {
							addEdge(this_AN, S+i);
						}
						
						// Edge from "MORE EXPENSIVE" FN
						if((FN_index-FN_start)>0) 			// not the first and most expensive AN <= UP RLM
							addEdge(this_AN,FN_index-1);	// more expensive FN is 
						
						// Edge to and from "SAME PRICE" FN (self edge ist somit enthalten)
						addEdge(this_AN,FN_index); 	
						addEdge(FN_index,this_AN);
						// Set weight = bid amount of this_AN (extended sum FN)
						try {
							FunctionNode fn = getFN(FN_index);
							int weight = (int)ansVec.get(this_AN).bid_amounts[tSlot]; 
							fn.addWeight(this_AN, weight);
						} catch (Exception error){
							Global.handleError(error, "Error setting weight for function node with index "+FN_index);
						}
						
						// save index of this_AN in moreexp_ANs vector to aggregate for cheaper ANs
						moreexp_ANs.add(this_AN);		 
					}					
					FN_index++;				
				}
				// Edges to "OVERALL SUM" FN for this timeslot
				Collections.sort(moreexp_ANs); 				// make sure neighbors are saved in ascending order
				for (Integer moreexp_AN: moreexp_ANs) {		// moreexp_ANs contains all ANs for this timeslot now
					addEdge(S+tSlot, moreexp_AN);
					// Set weight = bid amount of moreexp_AN (extended sum FN)
					try {
						FunctionNode fn = getFN(S+tSlot);
						int weight = (int)ansVec.get(moreexp_AN).bid_amounts[tSlot];
						fn.addWeight(moreexp_AN, weight);
					} catch (Exception error){
						int index = S+tSlot;
						Global.handleError(error, "Error setting weight for function node with index "+index);
					}
				}
				tSlot++;
			}
		}
	}
}
