package edu.stanford.multiagent.gamer.functions;


import edu.stanford.multiagent.gamer.*;

/**
 * An abstract class to support functions on vectors of values.
 */
public abstract class VectorFunction extends ParameterizedObject
{
    
    /**
     * Return the help screen
     */
    public String getHelp()
    {
        StringBuffer buff=new StringBuffer();
        buff.append(Global.wrap(getFunctionHelp(), 70));
        
        buff.append("\n\nFunction Parameters:\n");
        buff.append(Global.wrap(parameters.getPrintableInfo(), 70));
        
        
        return buff.toString();
    }
    
    /**
     * Returns a help string describing the function and the 
     * parameters taken by the function
     */
    protected  abstract String getFunctionHelp();

    /**
     * Constructor for a vector function.
     */
    public VectorFunction()
	throws Exception
    {
	super();
    }

    /**
     * Returns the arity of the function
     */
    /*public abstract int getArity();
    */
    public int getArity(){
      return -1;
    }

    /**
     * Returns the value of the function evaluated on x.
     *
     * @param x an array of values on which the function should
     * be executed
     */
    public abstract double eval(double[] x) throws Exception;


    /**
     * Initializes the vector function.
     */
    public void initialize()
	throws Exception
    {
	super.initialize();
    }
}
