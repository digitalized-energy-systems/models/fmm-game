package edu.stanford.multiagent.gamer.functions;

import edu.stanford.multiagent.gamer.*;

/* Function that returns the sum of its arguments
*/


public class SumFunction extends VectorFunction
{
    private static Parameters.ParamInfo[] sumParams;
   
    static{


	sumParams = new Parameters.ParamInfo[] {};
	Global.registerParams(SumFunction.class, sumParams);

    }

    protected  String getFunctionHelp()
    {
	return "SumFunction: a function that returns the sum of its arguments";
    }
    protected void checkParameters() 
    {
    }

  public SumFunction() 
	throws Exception
  {
	super();
  }
 
    public void initialize()
	throws Exception
    {
	super.initialize();
    }
    public void doGenerate()
    {
    }

    public double eval (double[] x )
    {
	double s=0;
	for (int i=0;i<x.length; i++){
          s=s+x[i];
	}
	return s;
    }

}
