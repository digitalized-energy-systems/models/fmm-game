package edu.stanford.multiagent.gamer.functions;

import java.util.*;
import edu.stanford.multiagent.gamer.*;

/* Function that returns the max of its arguments
*/


public class MaxFunction extends VectorFunction
{
    private static Parameters.ParamInfo[] maxParams;
   
    static{


	maxParams = new Parameters.ParamInfo[] {};
	Global.registerParams(MaxFunction.class, maxParams);

    }


    protected  String getFunctionHelp()
    {
	return "MaxFunction: a function that returns the max of its arguments";
    }
    protected void checkParameters() 
    {
    }

  public MaxFunction() 
	throws Exception
  {
	super();
  }
 
    public void initialize()
	throws Exception
    {
	super.initialize();
    }
    public void doGenerate()
    {
    }

    public double eval (double[] x ) throws Exception
    {
	if (x.length<1) throw new Exception("the vector should not be empty!");
	double s=x[0];
	for (int i=1;i<x.length; i++){
          s=Math.max(s,x[i]);
	}
	return s;
    }

}
