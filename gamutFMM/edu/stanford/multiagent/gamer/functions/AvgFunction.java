package edu.stanford.multiagent.gamer.functions;

import edu.stanford.multiagent.gamer.*;

public class AvgFunction extends VectorFunction
{
    private static Parameters.ParamInfo[] avgParams;
   
    static{


	avgParams = new Parameters.ParamInfo[] {};
	Global.registerParams(AvgFunction.class, avgParams);

    }


    protected  String getFunctionHelp()
    {
	return "AvgFunction: a function that returns the mean of its arguments";
    }
    protected void checkParameters() 
    {
    }


  public AvgFunction() throws Exception
  {
	super();
  }
 
    public void initialize()
	throws Exception
    {
	super.initialize();
    }
    public void doGenerate()
    {
    }

    public double eval (double[] x )
    {
	double s=0.0;
	for (int i=0;i<x.length; i++){
          s=s+x[i];
	}
	return s/x.length;
    }

}
