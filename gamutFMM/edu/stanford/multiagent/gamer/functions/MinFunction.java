package edu.stanford.multiagent.gamer.functions;

import java.util.*;
import edu.stanford.multiagent.gamer.*;

/* Function that returns the min of its arguments
*/


public class MinFunction extends VectorFunction
{
    private static Parameters.ParamInfo[] minParams;
   
    static{


	minParams = new Parameters.ParamInfo[] {};
	Global.registerParams(MinFunction.class, minParams);

    }


    protected  String getFunctionHelp()
    {
	return "MinFunction: a function that returns the min of its arguments";
    }
    protected void checkParameters() 
    {
    }

  public MinFunction() 
	throws Exception
  {
	super();
  }
 
    public void initialize()
	throws Exception
    {
	super.initialize();
    }
    public void doGenerate()
    {
    }

    public double eval (double[] x ) throws Exception
    {
	if (x.length<1) throw new Exception("the vector should not be empty!");
	double s=x[0];
	for (int i=1;i<x.length; i++){
          s=Math.min(s,x[i]);
	}
	return s;
    }

}
