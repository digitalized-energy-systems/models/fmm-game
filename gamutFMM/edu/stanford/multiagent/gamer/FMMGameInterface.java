/**
 * 
 */
package edu.stanford.multiagent.gamer;

/**
 * @author Gesa Ohlendorf
 *
 */
public interface FMMGameInterface {

	static final int MW = 1000; // kw
	static final int PRODUCT_TIME_SLICE = 4; // hours
	static final int ROUND_DIGITS = 4;
	
	/**
	 * 
	 * @param num_tSlots
	 * @param amount accepted bid amount in kW
	 * @param uprice uniform price per MW given in cent per hour
	 * @param MW_cost cost to hold 1 MW available for an hour in cent
	 * @param MWh_cost cost to provide 1 MW over an hour in cent
	 * @param pCall probability in [0,1] that accepted tenders have to
	 * 			    be provided over the full time span and bid amount
	 * @return payoff per time slot in cent
	 */
	default double getUtilityperSlot(int num_tSlots, double amount, double uprice, double MW_cost, double MWh_cost, double pCall){
		
		amount /= MW;
		uprice *= (double)PRODUCT_TIME_SLICE/(double)num_tSlots;			// get UP per timeslot (given per hour and MW)
		MW_cost *= (double)PRODUCT_TIME_SLICE/(double)num_tSlots;			// get MW_cost per timeslot (given per hour and MW)
		MWh_cost *= (double)PRODUCT_TIME_SLICE/(double)num_tSlots;			// get MWh_cost per timeslot (given per hour and MW)
		
		return amount*(uprice-MW_cost-MWh_cost*pCall);
	}
}
