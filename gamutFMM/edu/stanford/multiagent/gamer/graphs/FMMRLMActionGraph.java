package edu.stanford.multiagent.gamer.graphs;

import java.util.*;
import edu.stanford.multiagent.gamer.*;

/**
 * @author Gesa Ohlendorf
 *
 */
public class FMMRLMActionGraph extends ALGraph {
	
	private static class FNode
    {
		String pcKind;
		double[] bid_amounts;
    	int[] bid_prices;
    	
    	
    	public FNode(String pcKind, double[] bid_amounts, int[] bid_prices)
    	{
    		this.pcKind = pcKind;
    		this.bid_amounts = bid_amounts;
        	this.bid_prices=bid_prices;
    	}
    }

	/* Parameters
	 * 		
	 */
	protected static Parameters.ParamInfo pBoolNoBid;
	protected static Parameters.ParamInfo pActions;
	protected static Parameters.ParamInfo pFunctions;
	protected static Parameters.ParamInfo pFnsMap;
	private static Parameters.ParamInfo[] frbgParam;
	
	static
	{
		pBoolNoBid = new Parameters.ParamInfo("bool_noBid", Parameters.ParamInfo.BOOLEAN_PARAM, null, null, "Boolean whether Graph contains NO BID AN without any neighbors", true);
		pActions = new Parameters.ParamInfo("num_actions", Parameters.ParamInfo.LONG_PARAM, new Long(1), Long.MAX_VALUE, "number of ANs. at least 1", true);
		pFunctions = new Parameters.ParamInfo("num_functions", Parameters.ParamInfo.LONG_PARAM, new Long(0), Long.MAX_VALUE, "number of FNs", true);
		pFnsMap = new Parameters.ParamInfo("fnodes_map", Parameters.ParamInfo.HASHMAP_PARAM, null, null, "HashMap with FunctionNode objects as keys and Vectors with indices of ANs, that the FN aggregates, as value", true);
		
		frbgParam = new Parameters.ParamInfo[] {pBoolNoBid, pActions,pFunctions, pFnsMap};
		Global.registerParams(FMMRLMActionGraph.class, frbgParam);
	}
	
	public FMMRLMActionGraph() throws Exception {
		super();
	}
	
	public void initialize() throws Exception 
    {
		super.initialize();
		
		int S = 0; // #ANs
		int P = 0; // #FNs
		
		try {
			S = (int)parameters.getLongParameter(pActions.name);
			P = (int)parameters.getLongParameter(pFunctions.name);
		} catch (Exception e) {
			Global.handleError(e, "Unable to get parameters to " +
					   "initialize FMMGraph");
	    }
		
		if (nodes.isEmpty()) {
			for (int i = 0; i < S; i++) addNode();
			for (int i=S; i<S+P; i++) addFNode(i, FunctionNode.SUM, 0);
		}	
    }

	@Override
	protected String getGraphHelp() {
		return "Action graph for a FMM-RLM (B)AGG.";
	}

	@Override
	public boolean hasSymEdges() {
		return false;
	}

	@Override
	public boolean reflexEdgesOk() {
		return (true);	
	}

	@Override
	/** Generate Action Graph with calls of function addEdge(end, start)
	 *  Attention: make sure neighbors are added in ascending order
	 *  of indices (for both ANs and FNs)
	 */
	public void doGenerate() {
		
		// Get parameters needed
		boolean noBidAN = false;
		int S = 0; 
		int P = 0;
		HashMap<FNode, Vector<Integer>> fnsMap = new HashMap<FNode, Vector<Integer>>();
		try {
			noBidAN = parameters.getBooleanParameter(pBoolNoBid.name);
			S = (int)parameters.getLongParameter(pActions.name);
			P = (int)parameters.getLongParameter(pFunctions.name);
			fnsMap = parameters.getHashMapParameter(pFnsMap.name);
		} catch (Exception e) {
			Global.handleError(e, "Unable to get parameters to " +
								   "generate FMMRLMActionGraph");
	    }	
		
		Vector<Integer> aggrANs = new Vector<Integer>();
		
		// Add Edges to FNs
		int FNindex = S;
		for(Map.Entry<FNode, Vector<Integer>> set :
			fnsMap.entrySet()) {
			//FNode FN = set.getKey();
			Vector<Integer> inANs = set.getValue();
			Collections.sort(inANs); 
			for(int AN : inANs) {
				addEdge(FNindex,AN);
			}				
			FNindex++;
			aggrANs.addAll(inANs);
		}
		
		// Add Edges to ANs
		int i = noBidAN ? 1 : 0; // index of first AN to add edges to an from
		for(int AN=i;AN<S;AN++) {
			// Add Edges from all ans to all ANs
			for(int an=i;an<S;an++) {
				if(aggrANs.contains(an))
					continue; // but from those who are aggregated in FNs
				addEdge(AN,an);
			}
			// Add Edges from FNs to all ANs
			for (FNindex=S; FNindex<S+P; FNindex++)
				addEdge(AN,FNindex);
		}
	}
}