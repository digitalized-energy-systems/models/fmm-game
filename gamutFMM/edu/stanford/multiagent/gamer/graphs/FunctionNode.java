package edu.stanford.multiagent.gamer.graphs;

import java.util.*;

/**
 * @author Gesa Ohlendorf
 */

public class FunctionNode
{
	
	 public static final int SUM=0;
	 public static final int EXISTENCE=1;
	 public static final int HIGHEST_NON_ZERO=2;
	 public static final int LOWEST_NON_ZERO=3;
	 public static final int EXTENDED_SUM=10;
	 public static final int EXTENDED_EXISTENCE=11;
	 public static final int EXTENDED_HIGHEST_NON_ZERO=12;
	 public static final int EXTENDED_LOWEST_NON_ZERO=13;
	 
	 public static final int DEFAULT_WEIGHT = 1;
	  
	private int index;
	private int type;
    private int defaultval;
    private HashMap<Integer,Integer> weights;
	
	static {
		
	};

	/**
     * Constructor 
	 * @param t type of the function node
	 * @param d default value
	 * @param i index
	 */
	public FunctionNode(int i,  int t, int d) {
		index = i;
		type = t;
		defaultval = d;
		
		if(type<EXTENDED_SUM)
			weights = null;
		else
			weights = new HashMap();
	}	
	
	/**
	 * 
	 * @param from index of the incoming neighbor of FN
	 * @param weight of the specified neighbor
	 * @throws Exception if function node is not of an extended type
	 */
	public void addWeight(int from, int weight) throws Exception {
		if(this.type<EXTENDED_SUM)
			throw new Exception("Function Node error: FN of type "+type+" does not allow weights.");
		weights.put(from, weight);
	}
	
	/**
	 * 
	 * @param from index of the incoming neighbor of FN
	 * @return weight of the specified neighbor
	 */
	public int getWeight(int from) { //throws Exception {
		if(!weights.containsKey(from))
			return DEFAULT_WEIGHT; 	//throw new Exception("Function Node error: Weight for "+from+" was not found!"); // maybe an System.out.println() Warning?
		return weights.get(from);
	}
	
	/**
	 * 
	 * @return array with weights of all neighbors in the same
	 * order as neighbors are listed in action graph (ascending indices)
	 */
	public int[] getWeights() {
		int[] weight_arr = new int[weights.size()];
		
		int i = 0;
		Set<Integer> keySet = weights.keySet();
        for(Integer key : keySet)
        {
        	weight_arr[i] = weights.get(key);
        	i++;
        }
		return weight_arr;
	}
	
	/**
	 * Special workaround function to return and array containing not only the
	 * weights of all neighbors but to return an array of length |numActions|
	 * @param numActions number of ANs
	 * @return array with |numActions| weights, that is the weights of all neighbors 
	 * or 0 if AN at that index is not a neighbor of this FN
	 * @throws Exception if numActions defined does not match the indices of weights
	 * specified in the weights HashMap
	 */
	// Preliminary workaround to return |S| instead of |numneighbors| integers
	public int[] getWeights(int numActions) throws Exception {
		
		int[] weight_arr = new int[numActions]; // array with all elements initialized to 0 by default

		Set<Integer> keySet = weights.keySet();
        for(Integer key : keySet)
        	weight_arr[key] = weights.get(key);	// overwrite array elements with neighbors' weights

		return weight_arr;
	}
	
	public int getType() {
		return type;
	}

	public void setType(int t) {
		type = t;
	}
	
	public int getDefaultValue() {
		return defaultval;
	}
	
	
	
	public int combine(int x, int y) {
		return 0;
	}
	public int compareTo(FunctionNode fn) {
		return 0;
	}
	public boolean equals(Object o) {
		return false;
	}
	public boolean equals(FunctionNode fn) {
		return false;
	}
	public int hashCode() {
		return 0;
	}
	public final Object clone() {
		return null;
	}
	public int compareTo(Object o) {
		return 0;
	}
	static Integer access$1(FunctionNode fn) {  // ??
		return 0;
	}
	


	public static final class FunctionType {

 
	  static {
	  };
	  

	  public int getId() {
		return 0;
		  
	  }
	  public String toString() {
		return "";
	  }
	  public static FunctionType getDefault() {
		return null;
	  }
	  public static FunctionType getById(int id) {
		return null;
	  }
	  public static FunctionType[] values(){
		return null;
	  }
	  public static FunctionType valueOf(String s){
		return null;
	  }
	  static String access$2(FunctionType t) {
		return "";
	  }
	  
	}
}
