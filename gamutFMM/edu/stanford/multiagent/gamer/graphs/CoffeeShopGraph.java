package edu.stanford.multiagent.gamer.graphs;

import java.util.*;
import edu.stanford.multiagent.gamer.*;


/*
 * Generate the action graph for the Coffee Shop game generator.
 * 
 */

public class CoffeeShopGraph extends ALGraph
{

    // Parameters
    protected static Parameters.ParamInfo pRows;

    protected static Parameters.ParamInfo pCols;
    protected static Parameters.ParamInfo[] csgParam;

    static{

	pRows = new Parameters.ParamInfo("num_rows", Parameters.ParamInfo.LONG_PARAM, new Long(1), new Long(100), "Number of rows.  Must be > 0.  May be set up to 100."); 

	pCols= new Parameters.ParamInfo("num_cols", Parameters.ParamInfo.LONG_PARAM, new Long(1), new Long(100), "Number of columns.  Must be > 0.  May be set up to 100."); 



	csgParam = new Parameters.ParamInfo[] {pRows, pCols};

	Global.registerParams(CoffeeShopGraph.class, csgParam);

    }

    protected  String getGraphHelp()
    {
	return "CoffeeShopGraph: action graph for the coffee shop game.";
    }
    public CoffeeShopGraph() throws Exception
    {
	super();
    }


    public void initialize()
	throws Exception 
    {
		super.initialize();
		if(nodes.isEmpty()){
		
	
		    int nr= 0;
		    int nc= 0;
	
		    try {
			nr= (int) getLongParameter(pRows.name);
			nc= (int) getLongParameter(pCols.name);
		    } catch (Exception e) {
			Global.handleError(e, "Unable to get parameters to " +
					   "initialize CoffeeShopGraph");
		    }
	
		    // Former version before proper FunctionNode class was introduced:
		    /*long numNodes =  3* nr * nc  +1;
		    for (long i=0;i<numNodes;i++){
		      addNode();
		    }*/
		    //New version now that proper FunctionNode class is introduced:
		    int numANodes =  nr * nc  +1;
		    for (int i=0;i<numANodes;i++){
		      addNode();
		    }
		    int numPNodes =  nr * nc * 2;
		    for (int i=numANodes;i<numANodes+numPNodes;i++){
		    	addFNode(i, FunctionNode.SUM, 0);
		    }
		}
    }

    public boolean hasSymEdges() {
	return (false);
    }
    public boolean reflexEdgesOk() {
	return (true);
    }

    /**
     * Makes sure that the parameters are in the proper range
     */
    protected void checkParameters() throws Exception 
    {
	if (getLongParameter(pRows.name) <= 0)
	    throw new Exception("num_rows<= 0");

	if (getLongParameter(pRows.name) > 100)
	    throw new Exception("num_rows> 10");


	if (getLongParameter(pCols.name) <= 0)
	    throw new Exception("num_cols<= 0");

	if (getLongParameter(pCols.name) > 100)
	    throw new Exception("num_cols> 100");
    }



    public void doGenerate() {
	int[] dr ={-1,-1,-1,0,0,1,1,1}; 
	int[] dc ={-1,0,1,-1,1,-1,0,1};

	int nr=(int) getLongParameter(pRows.name);
	int nc=(int)  getLongParameter(pCols.name);
	int S = nr*nc +1;
	int P = 2*nr*nc;

	//action nodes' neighbors:
	for (int i=1;i<S;i++){
	  addEdge (i, i);
	  addEdge (i, i+nr*nc);
	  addEdge (i, i+2*nr*nc);
	}

	//function nodes' neighbors:
	//1. those representing adjacent locations
	  for (int i=0; i<nr; i++){
	    for (int j=0;j<nc;j++){
	      int node = i*nc+j +S;
	      for (int k=0;k<8;k++){
	        int i1=i+dr[k];
	        int j1= j+dc[k];
	        if(i1<0||i1>=nr || j1<0||j1>=nc) continue;
	        addEdge(node,i1*nc+j1+1);
	      }
	    }
	  }

	//2. those representing non-adjacent locations
	   for (int i=0; i<nr; i++)
	    for (int j=0;j<nc;j++)
	      for (int i1=0;i1<nr;i1++)
	        for (int j1=0;j1<nc;j1++){
	          int node = i*nc+j + S+nr*nc;
	          if (Math.abs (i-i1) >1 ||Math.abs(j-j1) >1)
	            addEdge(node,i1*nc+j1+1);
        
	        }  
	

    }



}
