package edu.stanford.multiagent.gamer.graphs;

import java.util.*;
import edu.stanford.multiagent.gamer.*;

/**
 * An Adjacency list implementation of the Graph interface.
 *
 */


public abstract class ALGraph extends Graph
{
    protected Vector nodes;
    protected HashMap<Integer,FunctionNode> fns;

    /**
     * Constructor
     */
    public ALGraph()
	throws Exception
    {
		super();
		nodes=new Vector();
		fns=new HashMap();
    }


    /**
     * Constructor which initializes the number of nodes.
     *
     * @param nNodes the number of nodes in the graph
     */
    public ALGraph(int nNodes)
	throws Exception
    {
	super();
	nodes = new Vector(nNodes);
	this.nNodes=nNodes;

	for(int i=0; i<nNodes; i++)
	    nodes.add(new LinkedList());
    }


    /**
     * Adds a new node to the graph.
     */
    public void addNode()
    {
		nNodes++;
		nodes.add(new LinkedList());
    }


    /**
     * Add a new edge to the graph.
     *
     * @param from the index of the node where the edge should start
     * @param to the index of the node where the edge should end
     */
    public void addEdge(int to, int from)
    {
      addEdge(to, from, null);
    }


    /**
     * Add an edge with data to the graph.
     *
     * @param from the index of the node where the edge should start
     * @param to the index of the node where the graph should end
     * @param data the data which is to be stored at the edge
     */
    public void addEdge(int to, int from, Object data) 
    {
      if(areNeighbours(to,from))
	return;

	LinkedList list = (LinkedList)nodes.get(to);
	Edge e=new Edge(to, from);
	e.setData(data);
	list.add(e);
	nEdges++;
    }
    
    /**
     * Add a function node
     * @param index of the node that is not a normal
     * action node but a function node
     */
    public void addFNode(int index) throws Exception {
    	addFNode(index, 0, 0);
    }
    
    /**
     * Add a function node
     * @param index of the node that is not a normal
     * action node but a function node
     * @param type of the FN (e.g. SUM, EXIST etc.)
     * @param defaultval default value the FNs config is added to
     * @throws Exception if neither a node with the index
     * specified already exists nor will it be the correct
     * index if a new node is added
     */
    public void addFNode(int index, int type, int defaultval) throws Exception {
    	
    	// In case this AN has not yet been added
    	if(nodes.size()<=index) {
    		if(nodes.size()==index)
    			addNode();
    		else throw new Exception("Function Node error: Index "+index+" is too high!");		
    	}
    	
    	FunctionNode fn=new FunctionNode(index, type, defaultval);
    	fns.put(index, fn);
    }
    
    /**
     * Returns a function node from the action graph.
     * 
     * @param index specifying the function node requested
     * @throws Exception 
     */
    public FunctionNode getFN(int index) throws Exception
    {
    	if(!fns.containsKey(index))
    		throw new Exception("Function Node error: FN with index "+index+" was not found!");
		return fns.get(index);
    }


    /**
     * Sets the data item for a preexisting edge.
     *
     * @param from the index of the node where the edge begins
     * @param to the index of the node where the edge ends
     * @param data the data which is to be stored at the edge
     */
    public void setEdgeData(int to, int from, Object data)
    {
	LinkedList list = (LinkedList)nodes.get(to);
	Iterator it = list.iterator(); 
	while(it.hasNext())
	    {
		Edge e = (Edge)it.next();
		if(e.getDest() == from)		// ATTENTION: definition of neighbours --> source
		    {
			it.remove();
			e.setData(data);
			list.add(e);
			return;
		    }
	    }
    }
    

    /**
     * Removes an edge from the graph.
     * 
     * @param from the index of the node where the edge begins
     * @param to the index of the node where the edge ends
     */
    public void removeEdge(int to, int from)
    {
	LinkedList list = (LinkedList)nodes.get(to);
	Iterator it=list.iterator(); 
	while(it.hasNext())
	    {
		Edge e = (Edge)it.next();
		if(e.getDest()==from)			// ATTENTION: definition of neighbours --> source
		    {
			it.remove();
			nEdges--;
			return;
		    }
	    }
    }

    /** 
     * Removes an edge from the graph.
     *
     * @param e the edge to be removed
     */
    public void removeEdge(Edge e)
    {
	LinkedList list = (LinkedList)nodes.get(e.getSource());		// ATTENTION: definition of neighbours --> source
	list.remove(e);
	nEdges--;
    }


    /**
     * Returns an Edge object from the graph.
     * 
     * @param from the index of the node where the edge begins
     * @param to the index of the node where the edge ends
     */
    public Edge getEdge(int to, int from)
    {
	LinkedList list = (LinkedList)nodes.get(to);
	Iterator it=list.iterator(); 
	while(it.hasNext())
	    {
		Edge e = (Edge)it.next();
		if(e.getDest()==from)		// ATTENTION: definition of neighbours --> source
		    return e;
	    }

	return null;
    }
    
    /**
     * Checks if two nodes are neighbours in the graph. Note that
     * direction matters in this check.
     * 
     * @param from the index of the node where the edge begins
     * @param to the index of the node where the edge ends
     */
    public boolean areNeighbours(int to, int from)
    {
	LinkedList list = (LinkedList)nodes.get(to);
	Iterator it=list.iterator(); 
	while(it.hasNext())
	    {
		Edge e = (Edge)it.next();
		if(e.getDest()==from)	// ATTENTION: definition of neighbours --> source.
		    return true;
	    }

	return false;
    }

    /**
     * Returns an iterator over the node's neighbours.
     *
     * @param to the index of the node whose neighbours should
     * be returned
     */
    public Iterator getNeighbours(int to)
    {
	LinkedList list = (LinkedList)nodes.get(to);
	final Iterator it = list.iterator();

	return new Iterator() {
		
		public void remove() { 
		    throw new UnsupportedOperationException("Graph Iterator");
		}
		
		public Object next() {
		    return new Integer(((Edge)it.next()).getDest());	// ATTENTION: definition of neighbours --> source
		}

		public boolean hasNext() {
		    return it.hasNext();
		}
	    };
    }


    /**
     * Returns an iterator over the incoming edges to a node.
     *
     * @param to the index of the node whose neighbours should
     * be returned
     */
    public Iterator getEdges(int to)
    {
	return ((LinkedList)nodes.get(to)).iterator();
    }


    /**
     * Return number of neighbours incoming to a node.
     *
     * @param to the index of the node
     */
    public int getNumNeighbours(int to)
    {
	return ((LinkedList)nodes.get(to)).size();
    }


    /**
     * May be implemented by subclasses to check parameters 
     * if any exist.
     */
    protected void checkParameters() throws Exception
    {
	return;
    } 

}


