package edu.stanford.multiagent.gamer;

import java.util.*;
import java.nio.IntBuffer;
import edu.stanford.multiagent.gamer.graphs.*;

/*
 *  Generates a random symmetric AGG
 * 
 * 
 */ 
public class RandomSymmetricAGG extends ActionGraphGame
{


    boolean randomize;
    // Parameters: Takes as parameters the graph to be used
    // with its parameters, and the number of actions (which
    // currently must be the same for all players).
    //

    private static Parameters.ParamInfo pGraph;
    private static Parameters.ParamInfo pGraphParams;
    private static Parameters.ParamInfo[] raggParam;
    static {

	pGraph = new Parameters.ParamInfo("graph", Parameters.ParamInfo.STRING_PARAM, null, null, "the name of the graph structure class to use");

	pGraphParams = new Parameters.ParamInfo("graph_params", Parameters.ParamInfo.CMDLINE_PARAM, null, null, "parameters to be handed off to the graph, must be enclosed in [].");

	raggParam = new Parameters.ParamInfo[] {Game.players,
					       Game.symActions, pGraph,
					       pGraphParams};
	Global.registerParams(RandomSymmetricAGG.class, raggParam);
    }

  



    public RandomSymmetricAGG() throws Exception
    {
	super();
    }


    /**
     * Set parameters for the AGG.  The parameters for the function 
     * and graph will be taken care of later.
     */
    public void setParameters(ParamParser p, boolean randomize)
	throws Exception
    {
	super.setParameters(p, randomize);	
	this.randomize=randomize;
    }


    public void initialize()
	throws Exception
    {
	// The ActionGraphGame class takes care of initializing
	// the number of players
	super.initialize();
	setNumPlayers((int)getLongParameter(Game.players.name));

	// Get number of players and actions
	parsePlayersSameNumberActions();


	numActionNodes = getNumActions(0);
	numPNodes=0;
	actionSets = new int[getNumPlayers()][];
	for (int i=0;i<getNumPlayers();i++){
	  actionSets[i] = new int[getNumActions(i)];
	  for(int j=0;j<getNumActions(i);j++){
	    actionSets[i][j] = j;
	  }
	}

	// Initialize the graph
	initGraph();
	// The number of actions is equal to the number of
	// nodes in the graph
	if( graph.getNNodes() != getNumActions(0) )
	  throw new Exception("Number of nodes in the graph must be the same as the number of actions!");


    }


    protected void initGraph()
    {
	String graphName = parameters.getStringParameter(pGraph.name);
	graph = (ALGraph) Global.getObjectOrDie(graphName,
						Global.GRAPH);

	ParamParser graphParams = 
	    parameters.getParserParameter(pGraphParams.name);
	try{
	  if(randomize)
	      {
		// -- manually set everything if randomizing
		graph.setParameter("nodes", new Long(getNumActions(0)), true);

		// -- try to override reflexivity/symmetry params
		try {graph.setParameter("reflex_ok", Boolean.TRUE, true);}
		catch (Exception e) {}
		
		try {graph.setParameter("sym_edges", Boolean.FALSE, true);}
		catch (Exception e) {}
	      }
	    
	  // -- set other parameters from command line.
	  graph.setParameters(graphParams, randomize);
	  graph.initialize();


	} catch (Exception e){
	  System.err.println(getHelp());
	  System.err.println(graph.getHelp());
	  e.printStackTrace();
	    Global.handleError(e, "Error initializing graph: ");

	}

    }

    protected void checkParameters() throws Exception 
    {
      // -- if need to randomize graphs parameters, then must
      // -- be able to set number of nodes
      if(randomize && parameters.isParamSet(pGraph.name))
	if(!Global.isPartOf(Global.GRAPH, getStringParameter(pGraph.name), 
			   "GraphWithNodeParam"))
	  throw new Exception("ERROR: Cannot randomize graph parameters unless graph is an instance of GraphWithNodeParam!");
    }


    //
    // Randomize parameters that were not set by the user
    //
    public void randomizeParameters() {

	try {
	    if(!parameters.setByUser(pGraph.name))
		parameters.setParameter(pGraph.name, 
					Global.getRandomClass(Global.GRAPH, "GraphWithNodeParam"));

	} catch (Exception e) {
	    Global.handleError(e, "Randomizing Graphs");
	}

	// Deal with the CMDLINE_PARAM
	super.randomizeParameters();
    }

    public void doGenerate()
    {

	setDescription("Random Symmetric Action Graph Game\n"  + getDescription());
	setName("Random Symmetric Action Graph Game");

	// Generate the graph for the game.
	graph.doGenerate();

	setDescription(getDescription() + "\nGraph Params:\n" + graph.getDescription());

	// fill the payoffs with random numbers
	double low = DEFAULT_LOW;
	double high = DEFAULT_HIGH;
	//Outcome outcome=new Outcome(getNumPlayers(), getNumActions() );
	//int[] D=new int[numActionNodes];
	TreeMap[] payoffMaps= new TreeMap[numActionNodes];
	for (int i=0;i<numActionNodes;i++){
	  payoffMaps[i]=new TreeMap();
	  graph.setNodeData(i, payoffMaps[i]);
	}

	/*
	for (outcome.reset(); outcome.hasMoreOutcomes();outcome.nextOutcome()){
	  int[] o = outcome.getOutcome();
	  for (int i=0;i<numActionNodes; i++) {
	    D[i]=0;
	  }
	  for (int i=0;i<getNumPlayers();i++){
	    int node = o[i]-1;  // action index start from 1
	    D[node]++;
	  }
	  for(int i=0;i<numActionNodes;i++){
	    if(D[i]>0){
		int[] key = new int[graph.getNumNeighbours(i)];
		int pos=0;
		Iterator edgeIter=graph.getEdges(i);
		while (edgeIter.hasNext()){
		  Edge e=(Edge) edgeIter.next();
		  int neighborNode= e.getDest();
		  key[pos]=D[neighborNode];
		  pos++;
		}
		double payoff=Global.randomDouble(low,high);
		payoffMaps[i].put(IntBuffer.wrap(key),new Double(payoff));
	    } //end if
	  } //end for(int i..
	}//end for(outcome..
	*/
	TreeSet[] configs = initConfigs();
	for (int node = 0; node < numActionNodes; node++)
	{
	    Iterator confIter = configs[node].iterator();
	    while (confIter.hasNext())
	    {
		IntBuffer tt = (IntBuffer)confIter.next();
		int[] key = (int[])tt.array();
		double payoff = Global.randomDouble(low, high);
		payoffMaps[node].put(IntBuffer.wrap((int[])key.clone()), new Double(payoff));
	    }
	}
    }

    protected String getGameHelp(){
	return "Generate a random symmetric Action Graph Game \n"
	  + "(symmetric as in symmetric game, not symmetric graph)\n";


    }
}
