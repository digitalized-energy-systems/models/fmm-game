package edu.stanford.multiagent.gamer;



import java.util.*;
import java.nio.IntBuffer;

import edu.stanford.multiagent.gamer.graphs.*;
import edu.stanford.multiagent.gamer.functions.*;

/*
 * Generates a Coffee Shop game represented as an AGG
 * 
 */ 
public class CoffeeShopGame extends ActionGraphGame
{
  boolean randomize;

  /* Parameters
   *
   */
  private static Parameters.ParamInfo farFunction;
  private static Parameters.ParamInfo farFunctionParams;

  private static Parameters.ParamInfo nearFunction;
  private static Parameters.ParamInfo nearFunctionParams;

  private static Parameters.ParamInfo homeFunction;
  private static Parameters.ParamInfo homeFunctionParams;

  private static Parameters.ParamInfo combineFunction;
  private static Parameters.ParamInfo combineFunctionParams;

  private static Parameters.ParamInfo pColumns;
  private static Parameters.ParamInfo pRows;
  private static Parameters.ParamInfo[] cofParam;

  static {
    farFunction=new Parameters.ParamInfo ("far_func",Parameters.ParamInfo.STRING_PARAM, null, null, "the name of the function class to use for locations far away");
    farFunctionParams = new Parameters.ParamInfo("far_func_params", Parameters.ParamInfo.CMDLINE_PARAM, null, null, "parameters to be handed off to the far function, must be enclosed in [].");

    nearFunction=new Parameters.ParamInfo ("near_func",Parameters.ParamInfo.STRING_PARAM, null, null, "the name of the function class to use for nearby locations");
    nearFunctionParams = new Parameters.ParamInfo("near_func_params", Parameters.ParamInfo.CMDLINE_PARAM, null, null, "parameters to be handed off to the near function, must be enclosed in [].");

    homeFunction=new Parameters.ParamInfo ("home_func",Parameters.ParamInfo.STRING_PARAM, null, null, "the name of the function class to use for the current location");
    homeFunctionParams = new Parameters.ParamInfo("home_func_params", Parameters.ParamInfo.CMDLINE_PARAM, null, null, "parameters to be handed off to the home function, must be enclosed in [].");


    combineFunction=new Parameters.ParamInfo ("combine_func",Parameters.ParamInfo.STRING_PARAM, null, null, "the name of the multivariate function class to use for combining the results of the far, near and home functions");
    combineFunctionParams = new Parameters.ParamInfo("combine_func_params", Parameters.ParamInfo.CMDLINE_PARAM, null, null, "parameters to be handed off to the combine function, must be enclosed in [].");

    pColumns=new Parameters.ParamInfo("columns", Parameters.ParamInfo.LONG_PARAM, new Long(1), new Long(100), "number of columns. Must be >=1 and <=100",true);

    pRows=new Parameters.ParamInfo("rows", Parameters.ParamInfo.LONG_PARAM, new Long(1), new Long(100), "number of rows. Must be >=1 and <=100",true);

    cofParam= new Parameters.ParamInfo[] {Game.players, pRows, pColumns,homeFunction, homeFunctionParams, nearFunction, nearFunctionParams, farFunction, farFunctionParams, combineFunction, combineFunctionParams};
    Global.registerParams(CoffeeShopGame.class , cofParam);
  }

  public CoffeeShopGame() throws Exception
  {
    super();
  }

  public void initialize() throws Exception
  {
	super.initialize();

	// Get number of players 
        if(!parameters.isParamSet(Game.players.name))
            Global.handleError("Required parameter missing: players");
	setNumPlayers((int) getLongParameter(Game.players.name));
	
	if(!parameters.isParamSet(pColumns.name))
	  Global.handleError("Required parameter missing: columns");
	if(!parameters.isParamSet(pRows.name))
	  Global.handleError("Required parameter missing: rows");

	numActionNodes = (int) (getLongParameter(pColumns.name) * getLongParameter(pRows.name) + 1);
	
	int[] a = new int[getNumPlayers()];
	for (int i=0;i<a.length; i++) a[i] = numActionNodes;
	setNumActions(a);
 
	//set number of func nodes

	numPNodes =(int) (getLongParameter(pColumns.name) * getLongParameter(pRows.name) * 2);
	actionSets= new int [getNumPlayers()][];
	for (int i=0;i<getNumPlayers();i++){
	  actionSets[i] = new int[getNumActions(i)];
	  for(int j=0;j<getNumActions(i);j++){
	    actionSets[i][j] = j;
	  }
	}

	//initialize the graph:
	initGraph();

	
  }

  protected void initGraph(){
    long nc =  getLongParameter(pColumns.name) ;
    long nr=   getLongParameter(pRows.name) ;

    try{
        graph = new CoffeeShopGraph ();

	graph.setParameter("num_rows", new Long(nr) , true);
	graph.setParameter("num_cols", new Long(nc),true);
	graph.initialize();
    }  catch (Exception e){
	  System.err.println(getHelp());
	  System.err.println(graph.getHelp());
	  e.printStackTrace();
	    Global.handleError(e, "Error initializing graph: ");

    }

  }

  protected void checkParameters() throws Exception 
  {


  }

  public void setParameters(ParamParser p, boolean randomize) 
	throws Exception
    {
	super.setParameters(p, randomize);	
	this.randomize=randomize;
    }


    //
    // Randomize parameters that were not set by the user
    //
  public void randomizeParameters() 
  {
	try{
	if(!parameters.setByUser(farFunction.name))
	    parameters.setParameter(farFunction.name, 
				    Global.getRandomClass(Global.FUNC));
	if(!parameters.setByUser(nearFunction.name))
	    parameters.setParameter(nearFunction.name, 
				    Global.getRandomClass(Global.FUNC));
	if(!parameters.setByUser(homeFunction.name))
	    parameters.setParameter(homeFunction.name, 
				    Global.getRandomClass(Global.FUNC));
	if(!parameters.setByUser(combineFunction.name))
	    parameters.setParameter(combineFunction.name, 
				    Global.getRandomClass(Global.VFUNC));


	} catch (Exception e) {
	    Global.handleError(e, "Randomizing Functions");
	}

	super.randomizeParameters();


  }

  public void doGenerate()
  {
	int n=getNumPlayers();

	setDescription("Coffee Shop Game represented as AGG\n"  + getDescription());
	setName("Coffee Shop Game");

	// Generate the graph for the game.
	graph.doGenerate();

        // Generate Payoffs

	String farName = parameters.getStringParameter(farFunction.name);
	ParamParser farParams = parameters.getParserParameter(farFunctionParams.name);

	String nearName = parameters.getStringParameter(nearFunction.name);
	ParamParser nearParams = parameters.getParserParameter(nearFunctionParams.name);

	String homeName = parameters.getStringParameter(homeFunction.name);
	ParamParser homeParams = parameters.getParserParameter(homeFunctionParams.name);

	String combineName = parameters.getStringParameter(combineFunction.name);
	ParamParser combineParams = parameters.getParserParameter(combineFunctionParams.name);

	Function Far =(Function) Global.getObjectOrDie(farName,
							      Global.FUNC);


	Function Near=(Function) Global.getObjectOrDie(nearName,
							      Global.FUNC);


	Function Home=(Function) Global.getObjectOrDie(homeName,
							      Global.FUNC);

	VectorFunction Combine=(VectorFunction) Global.getObjectOrDie(combineName,Global.VFUNC );


	try {
		Far.setDomain(0, getNumPlayers());
		Far.setParameters(farParams, randomize);
		Far.initialize();
		Far.doGenerate();
		Near.setDomain(0, getNumPlayers());
		Near.setParameters(nearParams, randomize);
		Near.initialize();
		Near.doGenerate();
		Home.setDomain(0, getNumPlayers());
		Home.setParameters(homeParams, randomize);
		Home.initialize();
		Home.doGenerate();
		Combine.setParameters(combineParams,randomize);
		Combine.initialize();
		Combine.doGenerate();

	} catch (Exception e) {
		System.err.println(getHelp());
		System.err.println(Far.getHelp());
		System.err.println(Near.getHelp());
		System.err.println(Home.getHelp());
		System.err.println(Combine.getHelp());
		Global.handleError(e, "Error initializing functions");
	}

	
	double u =0;

	for (int node=0; node<numActionNodes; node++){
	 TreeMap<IntBuffer,Double> payoffMap=new TreeMap<IntBuffer,Double>();
	 graph.setNodeData(node, payoffMap);

	 //action node 0 is for not entering the market, with utility 0
	 if (node==0){
	   u=0.0;
	   int[] key = new int[] {};
	   payoffMap.put(IntBuffer.wrap(key), new Double(u));
	   continue;
	 }
	 //other action nodes: evaluate the utilities, store in the TreeMap
	 // Diese drei for-Schleifen f�hren zu allen Configs
	 for(int i=1;i<=n; i++){ 		// for each player (HOME)
	  for(int j=0;i+j<=n;j++){		// for each other player (NEAR) (only combinations that haven't been looked before)
	   for(int k=0;i+j+k<=n;k++){	// (FAR)

	    Iterator it = graph.getNeighbours(node);
	    int homenode = (Integer)it.next();
	    int nearnode = (Integer)it.next();
	    int farnode = (Integer) it.next();
	    if (graph.getNumNeighbours(nearnode) == 0 && j > 0) continue;
	    if (graph.getNumNeighbours(farnode) == 0 && k > 0) continue;
	    double [] v = {Home.eval(i), Near.eval(j), Far.eval(k)};
	    try{
		u= Combine.eval(v);
	    } catch (Exception e) {
		Global.handleError(e,"Error evaluating functions");
	    }
	    int[] key =new int[] {i,j,k};
	    payoffMap.put(IntBuffer.wrap(key),new Double(u));
	   }
	  }
	 }
	}
  }

  protected String getGameHelp()
  {
	return "Generate a Coffee Shop Game.\n";
  }

}
