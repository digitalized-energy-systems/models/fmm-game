/**
 * 
 */
package edu.stanford.multiagent.gamer;

import java.nio.IntBuffer;
import java.util.*;
import java.util.stream.IntStream;

import org.apache.commons.math3.distribution.HypergeometricDistribution;

// import org.apache.commons.math3.distribution.HypergeometricDistribution;

import java.util.Map.Entry;

import edu.stanford.multiagent.gamer.graphs.*;


/**
 * @author Paul Hendrik Tiemann
 * Based on work of Gesa Ohlendorf
 * Generate the FMM-RLM game represented as a BAGG
 *
 */
public class FMMRLMBayesSplitGame extends BayesActionGraphGame implements FMMGameInterface {
	
	private static class ActionNode
    {
		String pcKind;
		double[] bid_amounts;
    	int[] bid_prices;
    	int[] MW_cost;
    	int[] MWh_cost;
    	
    	public ActionNode(String pcKind, double[] bid_amounts, int[] bid_prices, int[] MW_cost, int[] MWh_cost)
    	{
    		this.pcKind = pcKind;
    		this.bid_amounts = bid_amounts;
        	this.bid_prices=bid_prices;
        	this.MW_cost = MW_cost;
        	this.MWh_cost = MWh_cost;
    	}
    	
    	/**
    	 * Checks whether two ANs represent the same
    	 * action but with different utility functions
    	 * @param that AN to compare this to
    	 * @return true/false
    	 */
		public boolean sameAction(ActionNode that) {
			
	        if(this.pcKind.equals(that.pcKind) &&
	           Arrays.equals(bid_amounts,that.bid_amounts) &&
	           Arrays.equals(this.bid_prices, that.bid_prices))
	        	return true;
	        else
	        	return false;
		}
		
		@Override
    	/**
    	 * ActionNode objects are considered equal if they have the same values for all of their properties
    	 * (override so that equal doesn't require the memory address to be the same,
    	 *  that would otherwise only be the case for a comparison of an ActionNode object with itself)
    	 */
	    public boolean equals(Object o) {
			if (o == this) return true;
	        if (!(o instanceof ActionNode)) return false;
	        return(
	        	 this.pcKind.equals(((ActionNode)o).pcKind) &&
	 	         Arrays.equals(bid_amounts,((ActionNode)o).bid_amounts) &&
		         Arrays.equals(this.bid_prices,((ActionNode)o).bid_prices) &&
		         Arrays.equals(MW_cost,((ActionNode)o).MW_cost) &&
		         Arrays.equals(MWh_cost,((ActionNode)o).MWh_cost));
	    }
    	
    	@Override
    	/**
    	 * objects must be equal if and only if their hashcodes are equal
    	 */
        public int hashCode() {
    		return pcKind.hashCode()+Arrays.hashCode(bid_amounts)+Arrays.hashCode(bid_prices)+Arrays.hashCode(MW_cost)+Arrays.hashCode(MWh_cost);
        }
    }
	
	private static class FNode
    {
		String pcKind;
		double[] bid_amounts;
    	int[] bid_prices;
    	
    	
    	public FNode(String pcKind, double[] bid_amounts, int[] bid_prices)
    	{
    		this.pcKind = pcKind;
    		this.bid_amounts = bid_amounts;
        	this.bid_prices=bid_prices;
    	}
    	
    	@Override
    	/**
    	 * FNode objects are considered equal if they have the same values for all of their properties
    	 * (override so that equal doesn't require the memory address to be the same,
    	 *  that would otherwise only be the case for a comparison of a FNode object with itself)
    	 */
	    public boolean equals(Object o) {
    		if (o == this) return true;
	        if (!(o instanceof FNode)) return false;
	        return(
	        	 this.pcKind.equals(((FNode)o).pcKind) &&
	 	         Arrays.equals(bid_amounts,((FNode)o).bid_amounts) &&
		         Arrays.equals(this.bid_prices,((FNode)o).bid_prices));
	    }
    	
    	@Override
    	/**
    	 * objects must be equal if and only if their hashcodes are equal
    	 */
        public int hashCode() {
    		return pcKind.hashCode()+Arrays.hashCode(bid_amounts)+Arrays.hashCode(bid_prices);
        }

    }

	private static class Playerclass extends ParameterizedObject {
		
		String pcKind; // so far: either RLM or FMM
		int numPlayers;
		double[] typeDistr; // OBSOLETE (!?)
		HashMap<Integer, Type> types = new HashMap<Integer, Type>();
		
		protected static Parameters.ParamInfo p_numPlayers;
		protected static Parameters.ParamInfo pTypes;
		protected static Parameters.ParamInfo[] pcParam;
		
		static
		{
			p_numPlayers= new Parameters.ParamInfo("num_players", Parameters.ParamInfo.LONG_PARAM,new Long(1),Long.MAX_VALUE,"number of players per playerclass, at least 1",true);
			pTypes = new Parameters.ParamInfo("types", Parameters.ParamInfo.VECTOR_PARAM, null, null, "Vector with all given types enclosed in [].",true);
			
			pcParam = new Parameters.ParamInfo[] {p_numPlayers, pTypes};
			Global.registerParams(Playerclass.class, pcParam);
		}
		
		 /**
	     * The constructor.  Must be called by all subclasses.
	     * @ pcKind kind of playerclass (RLM, FMM)
	     * @throws Exception
	     */
		protected Playerclass(String pcKind) throws Exception {
			super();
			this.pcKind = pcKind;
		}
		
		/**
	     * Initializes Playerclass using preset parameter values
	     *
	     * @throws Exception
	     */
	    public void initialize() throws Exception
	    {
	    	// Get parameters
			this.numPlayers = (int)parameters.getLongParameter(p_numPlayers.name);
			Vector<Vector> typesVec = parameters.getVectorParameter(pTypes.name);			
			int i = 0;
			this.typeDistr = new double[typesVec.size()];
			for(Vector<HashMap> type : typesVec) {
				this.typeDistr[i] = addType(i, type.firstElement());
				i++;
			}			
			
			super.initialize();
	    }
	    
	    private double addType(int index, HashMap typarMap) throws Exception { // param type
    	   	
	    	Type t = new Type();
	    	ParamParser p=null;
	    	try {
	    	    p = new ParamParser(typarMap);
	    	    t.setParameters(p, false);
	    	} catch (Exception e) {
	    	    System.err.println(e.toString());
	    	    System.err.println(Global.getHelp());
	    	    System.exit(1);
	    	}
	    	t.initialize();	  
	    	
	    	types.put(index, t);
	    	return t.distr;
	    }
	    
	    private Type getType(int index) throws Exception
	    {
	    	if(!types.containsKey(index))
	    		throw new Exception("Type error: Type with index "+index+" was not found!");
			return types.get(index);
	    }
		
		@Override
		/**
		 * Check that type distributions adds up to 1
		 */
		protected void checkParameters() throws Exception {
			double distrSum = 0;
			for(Type Type : types.values()) {
				distrSum += Type.distr;
			}
			distrSum = Global.round(6,distrSum); // workaround double precision issue (could also use BigDecimal)
			if(distrSum != 1) {
				throw new IllegalArgumentException("Type distribution should but does not add up to 1 (sum is "+distrSum+")!");
			}
		}

		@Override
		public void doGenerate() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public String getHelp() {
			// TODO Auto-generated method stub
			return null;
		}
		
		protected static class Type extends ParameterizedObject {
			
			protected Type() throws Exception {
				super();
				// TODO Auto-generated constructor stub
			}

			protected static Parameters.ParamInfo p_distr;
			protected static Parameters.ParamInfo p_MWcost;
			protected static Parameters.ParamInfo p_MWhcost;
			protected static Parameters.ParamInfo p_Actions;
			private static Parameters.ParamInfo[] pcParam;
			
			double distr;
			int[] MW_cost;
			int[] MWh_cost;
			String[][] actions;
			
			static
			{
				p_distr = new Parameters.ParamInfo("distr", Parameters.ParamInfo.DOUBLE_PARAM, new Double(0), new Double(1),"probability in [0,1] of that type", true);
				p_MWcost = new Parameters.ParamInfo("MW_cost", Parameters.ParamInfo.STRING_PARAM,Long.MIN_VALUE,Long.MAX_VALUE,"standby cost for every MW per hour in cent,  1 integer per time slot dividided by comma, must be enclosed in [] ",true);
				p_MWhcost = new Parameters.ParamInfo("MWh_cost", Parameters.ParamInfo.STRING_PARAM,Long.MIN_VALUE,Long.MAX_VALUE,"provision cost for every called MW for every hour in cent, 1 integer per time slot dividided by comma, must be enclosed in [] ",true);
				p_Actions = new Parameters.ParamInfo("actions", Parameters.ParamInfo.VECTOR_PARAM, null, null, "Vector with all possible actions for this playerclass' type, must be enclosed in []",true);
				
				pcParam = new Parameters.ParamInfo[] {p_distr, p_MWcost, p_MWhcost, p_Actions};
				Global.registerParams(Type.class, pcParam);
			}
			
			/**
		     * Initializes Type using preset parameter values
		     *
		     * @throws Exception
		     */
		    public void initialize() throws Exception
		    {
		    	super.initialize(); // ruft checkParameters auf (ODER??)  
		    	
		    	// Get parameters
				this.distr = parameters.getDoubleParameter(p_distr.name);
				String mwString = parameters.getStringParameter(p_MWcost.name);
				this.MW_cost = Arrays.stream(mwString.split(",")).map(String::trim).mapToInt(Integer::parseInt).toArray();
				String mwhString = parameters.getStringParameter(p_MWhcost.name);
				this.MWh_cost = Arrays.stream(mwhString.split(",")).map(String::trim).mapToInt(Integer::parseInt).toArray();
				Vector<String> actionsVec = parameters.getVectorParameter(p_Actions.name);
				this.actions = new String[actionsVec.size()][];
				int i = 0;
				for(String a : actionsVec) {
					this.actions[i] = a.split(",");
					i++;
				}
		    }

			@Override
			protected void checkParameters() throws Exception {
				
			}

			@Override
			public void doGenerate() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public String getHelp() {
				// TODO Auto-generated method stub
				return null;
			}
		}	
	}
	
	private boolean noBidused=false;
	private int num_tSlots;
	private double pCall;
	private HashMap<Integer, Playerclass> pcs = new HashMap<Integer, Playerclass>();
	private Vector<ActionNode> ans = new Vector<ActionNode>();
	private LinkedHashMap<FNode, Vector<Integer>> fns = new LinkedHashMap<FNode, Vector<Integer>>();
	

	/* Parameters
	 *
	 */	
	private static Parameters.ParamInfo p_pCall;
	private static Parameters.ParamInfo p_numtSlots;
	private static Parameters.ParamInfo p_connected_RLM_tenderAmount;
	private static Parameters.ParamInfo p_split_RLM_tenderAmount;
	private static Parameters.ParamInfo p_split_FMM_tenderAmount;
	//private static Parameters.ParamInfo p_probability_split;
	private static Parameters.ParamInfo NatureplayerClassParams;
   	private static Parameters.ParamInfo RLMplayerClassParams;
   	private static Parameters.ParamInfo FMMplayerClassParams;
	private static Parameters.ParamInfo[] fmmrlmbParam;
	
	static
	{
		p_pCall = new Parameters.ParamInfo("p_call", Parameters.ParamInfo.DOUBLE_PARAM,new Double(0), new Double(1),"probability in [0,1] that accepted tenders have to be provided over the full time span and bid amount ",true);
		p_numtSlots= new Parameters.ParamInfo("num_tSlots", Parameters.ParamInfo.LONG_PARAM,Long.MIN_VALUE,Long.MAX_VALUE,"number of timeslots per product time slice",true);
		p_connected_RLM_tenderAmount = new Parameters.ParamInfo("rlm_tendAmount", Parameters.ParamInfo.LONG_PARAM,Long.MIN_VALUE,Long.MAX_VALUE,"amount of call for tenders at interconnected central balancing capacity market in MW",true);
		p_split_RLM_tenderAmount = new Parameters.ParamInfo("rlm_tendAmount_split", Parameters.ParamInfo.LONG_PARAM,Long.MIN_VALUE,Long.MAX_VALUE,"amount of call for tenders at separated central balancing capacity market in MW",true);
		p_split_FMM_tenderAmount = new Parameters.ParamInfo("fmm_tendAmount_split", Parameters.ParamInfo.LONG_PARAM,Long.MIN_VALUE,Long.MAX_VALUE,"amount of call for tenders at separated FMM market in MW",true);
		//p_probability_split = new Parameters.ParamInfo("split_probability", Parameters.ParamInfo.DOUBLE_PARAM, new Double(0), new Double(1), "probability of a system split with separated market clearing",true);
		NatureplayerClassParams = new Parameters.ParamInfo("Natureplayerclasses", Parameters.ParamInfo.NESTEDHASHMAP_PARAM, null, null, "probability of a system split with separated market clearing modelled as player.",true);
		RLMplayerClassParams = new Parameters.ParamInfo("RLMplayerclasses", Parameters.ParamInfo.NESTEDHASHMAP_PARAM, null, null, "parameters per playerclass of players bidding directly at the central balancing capacity market, must be enclosed in [].",true);
		FMMplayerClassParams = new Parameters.ParamInfo("FMMplayerclasses", Parameters.ParamInfo.NESTEDHASHMAP_PARAM, null, null, "parameters per playerclass, must be enclosed in [].",true);
		
		fmmrlmbParam = new Parameters.ParamInfo[] {p_pCall, p_numtSlots, p_connected_RLM_tenderAmount, p_split_RLM_tenderAmount, p_split_FMM_tenderAmount, NatureplayerClassParams, RLMplayerClassParams, FMMplayerClassParams};
		Global.registerParams(FMMRLMBayesSplitGame.class, fmmrlmbParam);
	}
	
	/**
	 * /**
	 * 
	 * @param index index of first player
	 * @param pcVec playerclass parameters
	 * @param market kind of playerclass (RLM, FMM)
	 * @return index of last player+1
	 * @return
	 * @throws Exception 
	 */
	private int addPClass(int index, Vector<HashMap> pcVec, String market) throws Exception {
    	   	
    	Playerclass pc = new Playerclass(market);
    	ParamParser p=null;

    	try {
    		if(pcVec.size()!=1)
    			throw new Exception("Error reading in playerclass parameters: unexpected number of parameters in "+pcVec);
    	    p = new ParamParser(pcVec.firstElement()); //nur 1 Element?!
    	    pc.setParameters(p, false);

    	} catch (Exception e) {
    	    System.err.println(e.toString());
    	    System.err.println(Global.getHelp());
    	    System.exit(1);
    	}
    	
    	pc.initialize();    	
    	pcs.put(index, pc);

    	return index+pc.numPlayers;
    }


	/**
	 * @throws Exception
	 */
	public FMMRLMBayesSplitGame() throws Exception {
		super();
	}
	
	public void initialize() throws Exception{
		
		super.initialize(); 
		
		/** Get parameters */
		num_tSlots = (int)parameters.getLongParameter(p_numtSlots.name);
		pCall = parameters.getDoubleParameter(p_pCall.name); 
		Vector<Vector> natureclasses = (Vector)parameters.getVectorParameter(NatureplayerClassParams.name).get(0);
		Vector<Vector> fmmpclasses = (Vector)parameters.getVectorParameter(FMMplayerClassParams.name).get(0);
		Vector<Vector> rlmpclasses = (Vector)parameters.getVectorParameter(RLMplayerClassParams.name).get(0);		
		
		// Initialize ActionNodes
		int[] zero = {0};
		double[] dzero = {0.0};
		final ActionNode an0 = new ActionNode("No Bid",dzero,zero,zero,zero);
		ans.add(an0);  // add AN 0 (NO BID)
		/*final ActionNode an1 = new ActionNode("Connected case",dzero,zero,zero,zero);
		ans.add(an1);  // add AN 1 (Connected case)
		final ActionNode an2 = new ActionNode("Split case",dzero,zero,zero,zero);
		ans.add(an2);  // add AN 2 (Split case)*/
		
		// Initialize PlayerClasses
		int numPlayers = 0;
		for(Vector natureclass : natureclasses) {
			numPlayers = addPClass(numPlayers, natureclass, "Nature");
		}
		for(Vector rlmpclass : rlmpclasses) {
			numPlayers = addPClass(numPlayers, rlmpclass, "RLM");
		}
		for(Vector fmmpclass : fmmpclasses) {
			numPlayers = addPClass(numPlayers, fmmpclass, "FMM");
		}
		
		/** Evaluate parameters to set (BAG)Game values */
		// Init BAGG values
		typeDistributions = new double[numPlayers][];	// distribution per player per type
		typeActionSets = new int[numPlayers][][];		// actions per player per type 
		actionSets = new int[numPlayers][];				// AN indices per player over all types, welcher Spieler welche AN nutzen kannst
		
		int p = 0;
		// for each playerclass
		for(Playerclass PC : pcs.values()) {
			
			int numTypes = PC.types.size();
			// for each player in PC
			for(int i=0; i<PC.numPlayers; i++) {
				typeDistributions[p+i] = new double[numTypes];
				typeActionSets[p+i] = new int[numTypes][];
			}
			
			// for each type of PC
			int t = 0;
			for(Playerclass.Type Type : PC.types.values()) {
				
				double distr = Type.distr;
				int[] MW_cost = Type.MW_cost;
				int[] MWh_cost = Type.MWh_cost;
				int numActions = Type.actions.length;
				// Check that number of given MW(h) costs is correct
				switch(PC.pcKind)
				{
				case "Nature":
					break;
				case "FMM":
					if(MW_cost.length != num_tSlots)
						Global.handleError("Unexpected Number of elements in " + PC.pcKind + "  MW_cost: " + Arrays.toString(MW_cost));
					if(MWh_cost.length != num_tSlots)
						Global.handleError("Unexpected Number of elements in " + PC.pcKind + "  MWh_cost: " + Arrays.toString(MWh_cost));
					break;
				case "RLM":
					if(MW_cost.length != 1)
						Global.handleError("Unexpected Number of elements in " + PC.pcKind + "  MW_cost: " + Arrays.toString(MW_cost));
					if(MWh_cost.length != 1)
						Global.handleError("Unexpected Number of elements in " + PC.pcKind + "  MWh_cost: " + Arrays.toString(MWh_cost));
					break;
				default:
					Global.handleError("Unknown kind of playerclass " + PC.pcKind);
			    }
				
				// for each player in PC
				for(int i=0; i<PC.numPlayers; i++) {
					typeDistributions[p+i][t] = distr;
					typeActionSets[p+i][t] = new int[numActions];
				}
				
				// for each action
				int a = 0;
				for(String[] action : Type.actions) {
					
					// Check that number of given bid amounts and prices is correct (and that bid amounts are a multiple of MW
					switch(PC.pcKind)
					{
					case "Nature":
						break;
					case "FMM":
						if(action.length != 2*num_tSlots)
							Global.handleError("Unexpected Number of elements in " + PC.pcKind + " action: " + Arrays.toString(action));
						break;
					case "RLM":
						if(action.length != 2)
							Global.handleError("Unexpected Number of elements in " + PC.pcKind + " action " + Arrays.toString(action));
						if((Integer.valueOf(action[0])%MW)>0)
							Global.handleError("Unexpected bid amount in action: " + Arrays.toString(action)+"! Bid amounts of RLM players must be 0 or multiples of 1 MW.");
						break;
					default:
						Global.handleError("Unknown kind of playerclass " + PC.pcKind);
				    }

					// check for "no bid" (whether all bid amounts are 0)
				    boolean bool_bid = false;
				    for(int m=0; m<action.length; m=m+2) {
				    	try {
					    	if(Integer.valueOf(action[m])!=0) {
					    		bool_bid = true;
					    		break;
					    	}
				    	} catch (Exception e) {
			    			Global.handleError(e, "Error reading in bid amount from action "+Arrays.toString(action)+"! Bid amounts and prices have to be Integer values!");
			    		}
				    }
				    // case no bid
				    if(!bool_bid) {
				    	noBidused=true;			    	
				    	// for each player in PC add "no bid"-AN 0 to type-action set
						for(int i=0; i<PC.numPlayers; i++) 
							typeActionSets[p+i][t][a] = 0; 
				    }			    	
				    else {
				    	// Get bid amounts and prices
				    	double[] bid_amounts = new double[action.length/2];
				    	int[] bid_prices = new int[action.length/2];
				    	for(int m=0; m<action.length/2; m++) {
				    		try {
					    		bid_amounts[m] = (double)Integer.valueOf(action[2*m]); // make sure bid amounts are given as integers, only handle as double internally
					    		if(bid_amounts[m]==0) // no bid for this time slot, so bid price doesn't matter
						    		bid_prices[m]=0;
						    	else
						    		bid_prices[m] = Integer.valueOf(action[2*m+1]);
				    		} catch (Exception e) {
				    			Global.handleError(e, "Error reading in bid amount from action "+Arrays.toString(action)+"! Bid amounts and prices have to be Integer values!");
				    		}
					    }
				    	// Check whether a corresponding AN is already listed
				    	ActionNode thisAN = new ActionNode(PC.pcKind,bid_amounts,bid_prices,MW_cost,MWh_cost);
				    	int ANindex = 0;
				    	// Reuse AN
				    	if(ans.contains(thisAN)) {
				    		Vector<Integer> ANindices = Global.getIndicesFromValue(ans, thisAN);
				    		if(ANindices.size()!=1)
				    			Global.handleError("Error setting ANs with indices: " + ANindices);
				    		ANindex = ANindices.get(0);
				    	}
				    	// Add AN to ans
				    	else {
				    		ANindex = ans.size();
				    		
				    		/* check whether there's any AN with same action but different
				    		 * utility function only to be aggregate via FN */
				    		for(ActionNode thatAN : ans) {
				    			if(thisAN.sameAction(thatAN)) {
				    				// thisAN and thatAN can be joined with FN
				    				final FNode thisFN = new FNode(PC.pcKind,bid_amounts,bid_prices);
				    				Vector<Integer> ANindices = new Vector<Integer>();
				    				// Check whether a corresponding FN is already listed
				    				if(fns.containsKey(thisFN)) 
				    					ANindices = fns.get(thisFN);
				    				else{
				    					// new fn, so index of thatAN has to be identified and added, too
				    					Vector<Integer> thatANindices = Global.getIndicesFromValue(ans, thatAN);  
							    		if(thatANindices.size()!=1)
							    			Global.handleError("Error setting ANs with indices: " + thatANindices);
				    					ANindices.add(thatANindices.get(0));
				    				}
				    				ANindices.add(ANindex);	 	// add this AN's index to Vector of incoming ANs
				    				fns.put(thisFN, ANindices); // safe in fns hashmap (replaces former entry if neccessary)				    				
				    				break;
				    			}
				    		}
				    		ans.add(thisAN);
				    	}
				    	// Update type-action set for each player of this PC
			    		for(int i=0; i<PC.numPlayers; i++) {
							// add this bid's AN (that is index ANindex) to type-action set
							typeActionSets[p+i][t][a] = ANindex;
						}
				    }
				    a++; // increment action index
				}
				// Make sure AN indices are saved in ascending order
				for(int i=0; i<PC.numPlayers; i++) 
					Arrays.sort(typeActionSets[p+i][t]);
				
				t++; //increment type index
			}
			
			// Get union of ANs for all players of this playerclass
			int[] union = new int[0];
			for(int j=0; j<numTypes; j++) {
				int[] current = typeActionSets[p][j];
				int[] merged = IntStream
				        .concat(IntStream.of(union), IntStream.of(current))
				        .distinct()
				        .sorted()
				        .toArray();
				union = merged;
			}
			Arrays.sort(union);
			// Save ANs for all players of this playerclass to setNumActions() later on
			for(int i=0; i<PC.numPlayers; i++) {
				actionSets[p+i] = union.clone(); 
			}
			// update player index
			p += PC.numPlayers;
		}
		
		// In case there's no NO BID (no AN 0)
		if(!noBidused) {
			
			ans.remove(0);
			
			// edit indices in (type)actionSets
			for(int i=0; i<numPlayers; i++) {
				for(int t=0; t<typeActionSets[i].length; t++) {
					for(int a=0; a<typeActionSets[i][t].length; a++)
					typeActionSets[i][t][a] = typeActionSets[i][t][a]-1;
				}				
				for(int j=0; j<actionSets[i].length; j++)
					actionSets[i][j] = actionSets[i][j]-1;
			}
			
			// edit indices of incoming ANs in fns
			for(Map.Entry<FNode, Vector<Integer>> entry : fns.entrySet()){
				
				Vector<Integer> inANs = entry.getValue();
				for(int i=0; i<inANs.size(); i++)
					inANs.set(i, inANs.get(i)-1);
				fns.put(entry.getKey(), inANs);
			}
		}
			
		/** Set (BAG)Game values */
		setNumPlayers(numPlayers);
		// set numANs
		numActionNodes = ans.size();
		// set numFNs
		numPNodes = fns.size();
		// set typesets (type distributions of each player)
		/* see above: typeDistributions[][] */
		// set size of type-action set for each player's each type
		/* see above: typeActionSets[][][] */
		// Sort type-action for each player's each type.
		/* see above: typeActionSets[][][] */
		// set setNumActions
		int[] a = new int[numPlayers];
		for (int i=0;i<a.length; i++) a[i] = actionSets[i].length;
		setNumActions(a);
		
		/** Initialize the graph */
		initGraph();
		
	}
	
	/**
	 * Same as in FMMRLMGame
	 */
	protected void initGraph(){
		
		try
		{
			graph = new FMMRLMActionGraph(); 
			graph.setParameter("bool_noBid", noBidused, true);
			graph.setParameter("num_actions", new Long(numActionNodes), true);
			graph.setParameter("num_functions", new Long(numPNodes), true);
			graph.setParameter("fnodes_map", fns, true);
			
			graph.initialize();

		}
		catch (Exception e)
		{
			System.err.println(getHelp());
			System.err.println(graph.getHelp());
			e.printStackTrace();
			Global.handleError(e, "initializing graph");
		}
	 }

	@Override
	protected void checkParameters() throws Exception {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Same as in FMMRLMGame
	 */
	private HashMap<Integer, Double> updateMOLMap(Object neiNode, HashMap<Integer, Double> rLM_hmap, int neiAss) {
		if(neiNode instanceof ActionNode)
			return updateMOLMap((ActionNode)neiNode, rLM_hmap, neiAss);
		else if(neiNode instanceof FNode) {
			return updateMOLMap(new ActionNode(((FNode)neiNode).pcKind, ((FNode)neiNode).bid_amounts, ((FNode)neiNode).bid_prices, null, null), rLM_hmap, neiAss);
		}
		return null;
	}
	
	/**
	 * Same as in FMMRLMGame
	 */
	private HashMap<Integer, Double> updateMOLMap(ActionNode neiNode, HashMap<Integer, Double> RLM_hmap, int neiAss) {
		double bid_amount = neiNode.bid_amounts[0] * neiAss;	// bid amount multiplied with AN assignment
		int bid_price = neiNode.bid_prices[0];
		if(RLM_hmap.containsKey(bid_price)) {
			bid_amount += RLM_hmap.get(bid_price);
		}
		RLM_hmap.put(bid_price, bid_amount);
		return RLM_hmap;
	}
	
	/**
	 * Same as in FMMRLMGame
	 */
	private HashMap<Integer, Vector<Double>> updateMOLMap(HashMap<Integer, Vector<Double>> FMM_hmap, Object neiNode, int neiAss) {
		if(neiNode instanceof ActionNode)
			return updateMOLMap(FMM_hmap, (ActionNode)neiNode, neiAss);
		else if(neiNode instanceof FNode) {
			return updateMOLMap(FMM_hmap, new ActionNode(((FNode)neiNode).pcKind, ((FNode)neiNode).bid_amounts, ((FNode)neiNode).bid_prices, null, null), neiAss);
		}
		return null;
	}
	
	/**
	 * Same as in FMMRLMGame
	 */
	private HashMap<Integer, Vector<Double>> updateMOLMap(HashMap<Integer, Vector<Double>> FMM_hmap,ActionNode neiNode, int neiAss) {

		for(int slot=0;slot<num_tSlots;slot++) {
			
			double bid_amount = neiNode.bid_amounts[slot] * neiAss;	// bid amount multiplied with AN assignment
			int bid_price = neiNode.bid_prices[slot];
			
			Vector<Double> amount_per_slotVEC = new Vector<Double>();
			if(FMM_hmap.containsKey(bid_price)) {
				amount_per_slotVEC = FMM_hmap.get(bid_price);
			}
			else
			{	// Init new vector
				for(int i=0;i<num_tSlots;i++)
					amount_per_slotVEC.add(0.0);
			}	
			double examount = amount_per_slotVEC.get(slot);
			amount_per_slotVEC.set(slot, bid_amount+examount); // replace value at index "slot": sum up all bid amounts for this price and timeslot in vector entry
				
			FMM_hmap.put(bid_price, amount_per_slotVEC);
		}
		return FMM_hmap;
	}


	@Override
	public void doGenerate() {
		
		setDescription("FMM_RLM Split Game represented as AGG\n" + getDescription());
		setName("FMM_RLM Split-AGG");
		
		/** Generate the graph for the game. */
		graph.doGenerate(); 
		
		/** Generate payoffs for each AN */
		int connected_RLM_tendAmount = MW*(int)parameters.getLongParameter(p_connected_RLM_tenderAmount.name); // get tenderAmount in kW
		int split_RLM_tendAmount = MW*(int)parameters.getLongParameter(p_split_RLM_tenderAmount.name); // get tenderAmount in kW
		int split_FMM_tendAmount = MW*(int)parameters.getLongParameter(p_split_FMM_tenderAmount.name); // get tenderAmount in kW
		// generate all configurations
		TreeSet [] configs = initConfigs();
		
		int node = -1;
		for(ActionNode origAN : ans) {
			node++;
			
			TreeMap<IntBuffer, Double> payoffMap = new TreeMap<IntBuffer, Double>();
			graph.setNodeData(node, payoffMap);
			Iterator confIter = configs[node].iterator();
			
			// for each config
			while (confIter.hasNext()){	
				IntBuffer tt = (IntBuffer)confIter.next();	 
				int[] config = (int[])tt.array();
				
				// clone origAN here, so changes made to thisAN won't affect further operations on other configs
				ActionNode thisAN = new ActionNode(origAN.pcKind, origAN.bid_amounts.clone(), origAN.bid_prices.clone(), origAN.MW_cost, origAN.MWh_cost);
				double u=0.0; 	// init utility for this AN and configuration
				
				if(node>2) { 	// else No bid (0) or Nature (1,2) --> u=0 (Attention: noBidused is omitted)
					/** 1. Create MOL both for FMM and overall RLM */
					
					// MOLs: HashMaps with key:price, value:amount (per slot)
					HashMap<Integer, Double> RLM_hmap = new HashMap<Integer, Double>();
					HashMap<Integer, Vector<Double>> FMM_hmap = new HashMap<Integer, Vector<Double>>();
					
					// For each neighbor node
					Iterator edgeIter = graph.getEdges(node);		
					for (int idx = 0; edgeIter.hasNext(); idx++){
						
						Edge e = (Edge)edgeIter.next();
						
						if(config[idx]==0)		// no contribution from this neighbor node
							continue;
						
						// Get neighbor node
						int nei = e.getDest(); 
						Object NeiNode;
						if(nei<numActionNodes)
							NeiNode = ans.get(nei);
						else
							NeiNode = fns.keySet().toArray()[nei-numActionNodes]; 

						//if(nei<numActionNodes && ((ActionNode)NeiNode).pcKind=="Nature")
						//	System.out.println(idx+": "+((ActionNode)NeiNode).pcKind);
						
						// Update MOL

						if(nei<numActionNodes && ((ActionNode)NeiNode).pcKind=="RLM" || (nei>=numActionNodes && ((FNode)NeiNode).pcKind=="RLM"))
							RLM_hmap = updateMOLMap(NeiNode, RLM_hmap, config[idx]);
						else
							FMM_hmap = updateMOLMap(FMM_hmap, NeiNode, config[idx]);
					}

					if (config[0]==1 && config[1]==0) { //Connected case (RLM+FMM)

						/** 2. Iterate over ascending, combined FMM-RLM-MOL to identify aggregated FMM bids and the overall UP */
						// Init...
						int UP=-1; 						//...resulting central market uniform price
						double overall_bidamount=0.0; 	//...amount of bids made at central market
						double UPbids=0.0;				//...amount of UP-bids made at central market 
						double surplus=0.0;				//...surplus amount of UP-bids
						
						//...values for FMM bid aggregation
						Vector<Double> aggrVec = new Vector<Double>(); // vector to aggregate kW per slot
						
						//...values for case: thisAN.pcKind=="FMMbid":
						// 		HashMap-Array to save FMM bids this AN is aggregated in per slot
						// 		-key: bidprice,	-value: Vector containing this AN's bid amount in each of the aggregated MWs at this price
						HashMap<Integer,Vector<Double>>[] fbidsmap = new HashMap[num_tSlots];
						// Vector-Array to save bid amounts of this AN in several-MW-bids per slot
						Vector<Double>[] thisANaggrVecarr = new Vector[num_tSlots]; 
						
						//...all arrays and vectors per slot
						for(int slot=0; slot<num_tSlots; slot++) {
							aggrVec.add(0.0);
							fbidsmap[slot] = new HashMap<Integer,Vector<Double>>();
							thisANaggrVecarr[slot] = new Vector<Double>();
						}
											
						// Combine MOLs
						Set<Integer> prices = new TreeSet<Integer>(RLM_hmap.keySet());
				        prices.addAll(FMM_hmap.keySet());
						for(int price : prices) {
							
							UPbids = 0.0; // reset
							
							// 1. check for RLM bid
							if(RLM_hmap.containsKey(price)) {
								overall_bidamount += RLM_hmap.get(price);
								UPbids += RLM_hmap.get(price);
							}
							
							// 2. check for aggregated FMM bid
							if(FMM_hmap.containsKey(price)) {
								
								Vector<Double> newVec = FMM_hmap.get(price);
								
								// ### SPECIAL CASE: if thisAN is a FMM-AN ###
								boolean bool_aggr = false; 
								if(thisAN.pcKind=="FMM") {	
									//check whether this AN's bid price is cheaper or equal to this price and therefore 
									//MAY be aggregated in a newly aggregated MW in any slot
									for(int slot=0; slot<num_tSlots; slot++) {
										if(thisAN.bid_prices[slot]<=price) {
											bool_aggr = true;
											break;
										}
									}
								}
								
								for(int slot=0; slot<num_tSlots; slot++) {
									
									double exAmount = aggrVec.elementAt(slot); 	// get kW already aggregated in this slot
									double addAmount = newVec.get(slot); 		// get additional internal FMM bid amount at this price
									
									if(addAmount>0) {
									
										// update aggregated kW
										aggrVec.set(slot,exAmount+addAmount);
										
										// ### SPECIAL CASE: if thisAN is a FMM-AN ###
										// then save values to calculate this AN's utility later on
										if((thisAN.pcKind=="FMM")&&bool_aggr) {							
											
											// If new MW(s) have been aggregated for this slot, add this AN's amount per MW to this slot's Vector
											int aggrMW = (int) ((exAmount+addAmount-(exAmount+addAmount)%MW)/MW); 
											while(aggrMW > thisANaggrVecarr[slot].size()) {
												
												double thisAmount=0.0; // init: new MW aggregated in this slot may be aggregated without thisAN
												
												if(thisAN.bid_prices[slot]<=price) {
													
													thisAmount = thisAN.bid_amounts[slot];
													double openTender = aggrMW*MW-exAmount;
													// in case several MW(s) are aggregated, thisAmount is split up to them all
													if(openTender>MW) {
														if(openTender%MW !=0)
															openTender = openTender%MW; // get open tender for first MW
														else
															openTender = MW;
													}
													
													if(thisAN.bid_prices[slot]==price && openTender<addAmount)
														// this AN's bid is partially aggregated in this MW
														thisAmount*=openTender/addAmount;
													
													// update remaining kW for internal aggregation
													thisAN.bid_amounts[slot] -= thisAmount;
													exAmount += openTender;
													addAmount -= openTender;
												}
												
												thisANaggrVecarr[slot].add(thisAmount);
											}
										}
										// ### END SPECIAL CASE ###
									}
								}
								
								// check for aggregated MWs
								double aggrBid = Collections.min(aggrVec);	// minimum amount over all timeslots
								aggrBid -= aggrBid%MW; 					// rounded down to MW (1000kW)
								
								// if FMM has aggregated MW(s)
								if(aggrBid>0){
									
									// update amounts of UP-bids and bids made overall
								 	UPbids += aggrBid;
									overall_bidamount += aggrBid;
									
									for(int slot=0;slot<num_tSlots;slot++) {  
										
										// update not yet aggregated FMM bid amounts
										aggrVec.set(slot, aggrVec.elementAt(slot)-aggrBid);
									
										// ### SPECIAL CASE: if thisAN is a FMM-AN ### 
										// add Vector containing this AN's bid amount in each of the aggregated MWs at this price to fbidsMap
										if(!thisANaggrVecarr[slot].isEmpty()&& (Collections.max(thisANaggrVecarr[slot])>0)) {
											
											// Check whether all of this AN has been aggregated in this bid
											if(aggrBid/MW < thisANaggrVecarr[slot].size()) {
												
												Vector<Double> thisBidaggrVec = new Vector<Double>();
												
												for(int i=0; i<aggrBid/MW; i++) {
													thisBidaggrVec.add(thisANaggrVecarr[slot].firstElement());
													thisANaggrVecarr[slot].removeElementAt(0);												
												}
												fbidsmap[slot].put(price,thisBidaggrVec);
												continue;
												
											}
											fbidsmap[slot].put(price,thisANaggrVecarr[slot]);
											thisANaggrVecarr[slot] = new Vector<Double>(); // reset Vector
										}
										// ### END SPECIAL CASE ###
									}
								}
							}
							
							// check whether market can be cleared
							if(overall_bidamount>=connected_RLM_tendAmount) {
								UP = price;
								surplus = overall_bidamount - connected_RLM_tendAmount;
								break;
							}
						}
						
						if(UP==-1) {
							for (int i: config) {
							  System.out.print(i);
							  System.out.print(" ");
							}
							System.out.println();
							Global.handleError("Error calculating uniform price: The tender amount was not matched for this config (see above) of node "+node);
						}
						
						/** 3. Calculate thisAN's utility */
						switch(thisAN.pcKind)
						{
						case "RLM":
							// Calculate utility for all time slots in one
							if(thisAN.bid_prices[0]<=UP) {
								
								double accAmount = thisAN.bid_amounts[0];
								
								if((thisAN.bid_prices[0]==UP)&&(surplus>0)){
									// In case of price equality at central RLM market, bids are accepted randomly
									// Expected utility for random acceptance is equal to expected utility for pro rata acceptance
									accAmount = (UPbids-surplus)*thisAN.bid_amounts[0]/UPbids; //prorataAmount = openTender*thisBid/allBids;
								}
								
								u = num_tSlots*getUtilityperSlot(num_tSlots,accAmount,(double)UP,thisAN.MW_cost[0],thisAN.MWh_cost[0],pCall); // getUtility returns utility per timeslot, multiply with number of timeslots()
							} // else: this node's bid is not accepted -> u=0.0
							break;
							
						case "FMM":
																			
							// Calculate utility per time slot
							for(int slot=0; slot<num_tSlots; slot++) {
								
								// Get HashMap of FMM bids this AN is aggregated in in this time slot
								if(!fbidsmap[slot].isEmpty()) {
									
									for(int price : fbidsmap[slot].keySet()) {
										
										// Calculate accepted bid amount
										double accAmount = 0.0;
										Vector<Double> accVec = fbidsmap[slot].get(price);
										
										// In case of price equality at central RLM market, bids are accepted randomly:
										if(price==UP && surplus>0) {
											
											int numMW = accVec.size();
											
											if(numMW==1) {
												// FMM has only aggregated 1 MW
												// Expected utility for random acceptance is equal to expected utility for pro rata acceptance
												accAmount = (UPbids-surplus)*accVec.get(0)/UPbids;
												u += getUtilityperSlot(num_tSlots,accAmount,(double)UP,thisAN.MW_cost[slot],thisAN.MWh_cost[slot],pCall);
											}
											else {
												// FMM has aggregated more than 1 MW with UP
												// Despite random acceptance, internal bid prices per FMM_UP-bid have to be considered (cheapest are aggregated first)
												int allBids = (int)UPbids/MW;				// aka population size
												int openTender = (int)(UPbids-surplus)/MW; 	// aka number of samples
												HypergeometricDistribution Dhyper = new HypergeometricDistribution(allBids, numMW, openTender);
												
												// for x=1...numMW MW calculate the probability for the FMM to get a tender accept for up to x MW
												// and multiply it with the utility for this AN's bid amount, that is aggregated in MW x
												for(int x=1; x<=numMW; x++) {
													
													accAmount = accVec.get(x-1); 
													
													if(accAmount==0.0) 		 
														continue;
													
													final double prob = Dhyper.upperCumulativeProbability(x);
													
													u += prob * getUtilityperSlot(num_tSlots,accAmount,(double)UP,thisAN.MW_cost[slot],thisAN.MWh_cost[slot],pCall);
												}
											}
											
										}
										// FMM bid is fully accepted:
										else if(price<=UP) {
											
											for(int i=0; i<accVec.size(); i++)
												accAmount += accVec.get(i);
											
											u += getUtilityperSlot(num_tSlots,accAmount,(double)UP,thisAN.MW_cost[slot],thisAN.MWh_cost[slot],pCall); 
										}
									}
								} // else: this node's bid is not accepted -> u+=0.0
							}
							break;
						
						default:
							Global.handleError("Error calculating payoffs: Unknown kind of playerclass " + thisAN.pcKind + " for ActionNode with index "+node);
					    }	
					} else if(config[0]==0 && config[1]==1) { // Split case
						//u = Math.random()*5;
						//u = thisAN.bid_prices[0]-thisAN.MW_cost[0]-thisAN.MWh_cost[0]*0.5;
						
						switch(thisAN.pcKind) {
						case "RLM": //RLM-player
							/** 2. Iterate over ascending, RLM-MOL to identify the overall UP */
							// Init...
							int UP=-1; 						//...resulting central market uniform price
							double overall_bidamount=0.0; 	//...amount of bids made at central market
							double overall_UPbids=0.0;				//...amount of UP-bids made at central market 
							double overall_surplus=0.0;				//...surplus amount of UP-bids
							
							//...values for FMM bid aggregation
							Vector<Double> overall_aggrVec = new Vector<Double>(); // vector to aggregate kW per slot
							
							//...values for case: thisAN.pcKind=="FMMbid":
							// 		HashMap-Array to save FMM bids this AN is aggregated in per slot
							// 		-key: bidprice,	-value: Vector containing this AN's bid amount in each of the aggregated MWs at this price
							HashMap<Integer,Vector<Double>>[] overall_fbidsmap = new HashMap[num_tSlots];
							// Vector-Array to save bid amounts of this AN in several-MW-bids per slot
							Vector<Double>[] thisANaggrVecarr = new Vector[num_tSlots]; 
							
							//...all arrays and vectors per slot
							for(int slot=0; slot<num_tSlots; slot++) {
								overall_aggrVec.add(0.0);
								overall_fbidsmap[slot] = new HashMap<Integer,Vector<Double>>();
								thisANaggrVecarr[slot] = new Vector<Double>();
							}
												
							// Set up MOL
							Set<Integer> RLM_prices = new TreeSet<Integer>(RLM_hmap.keySet());
							for(int price : RLM_prices) {
								
								overall_UPbids = 0.0; // reset
								
								// 1. check for RLM bid
								if(RLM_hmap.containsKey(price)) {
									overall_bidamount += RLM_hmap.get(price);
									overall_UPbids += RLM_hmap.get(price);
								}
										
								// check whether market can be cleared
								if(overall_bidamount>=split_RLM_tendAmount) {
									UP = price;
									overall_surplus = overall_bidamount - split_RLM_tendAmount;
									break;
								}
							}
							
							if(UP==-1) {
								for (int i: config) {
								  System.out.print(i);
								  System.out.print(" ");
								}
								System.out.println();
								Global.handleError("Error calculating uniform price: The tender amount was not matched for this config (see above) of node "+node);
							}
							
							/** 3. Calculate thisAN's utility */

							// Calculate utility for all time slots in one
							if(thisAN.bid_prices[0]<=UP) {
								
								double accAmount = thisAN.bid_amounts[0];
								
								if((thisAN.bid_prices[0]==UP)&&(overall_surplus>0)){
									// In case of price equality at central RLM market, bids are accepted randomly
									// Expected utility for random acceptance is equal to expected utility for pro rata acceptance
									accAmount = (overall_UPbids-overall_surplus)*thisAN.bid_amounts[0]/overall_UPbids; //prorataAmount = openTender*thisBid/allBids;
								}
								
								u = num_tSlots*getUtilityperSlot(num_tSlots,accAmount,(double)UP,thisAN.MW_cost[0],thisAN.MWh_cost[0],pCall); // getUtility returns utility per timeslot, multiply with number of timeslots()
							} // else: this node's bid is not accepted -> u=0.0
							break;
						case "FMM": // FMM-player
							/** 2. Iterate over ascending, FMM-MOL to identify the local UP */
							// Init...
							int local_UP=-1; 						//...resulting FMMuniform price
							double local_bidamount=0.0; 	//...amount of bids made at FMM
							double local_UPbids=0.0;				//...amount of UP-bids made at FMM 
							double local_surplus=0.0;				//...surplus amount of UP-bids
							
							//...values for FMM bid aggregation
							Vector<Double> aggrVec = new Vector<Double>(); // vector to aggregate kW per slot
							
							//...values for case: thisAN.pcKind=="FMMbid":
							// 		HashMap-Array to save FMM bids this AN is aggregated in per slot
							// 		-key: bidprice,	-value: Vector containing this AN's bid amount in each of the aggregated MWs at this price
							HashMap<Integer,Vector<Double>>[] FMM_fbidsmap = new HashMap[num_tSlots];
							// Vector-Array to save bid amounts of this AN in several-MW-bids per slot
							Vector<Double>[] FMM_thisANaggrVecarr = new Vector[num_tSlots]; 
							
							//...all arrays and vectors per slot
							for(int slot=0; slot<num_tSlots; slot++) {
								aggrVec.add(0.0);
								FMM_fbidsmap[slot] = new HashMap<Integer,Vector<Double>>();
								FMM_thisANaggrVecarr[slot] = new Vector<Double>();
							}
												
							// Set up MOL
							Set<Integer> FMM_prices = new TreeSet<Integer>(FMM_hmap.keySet());
							for(int price : FMM_prices) {
								
								local_UPbids = 0.0; // reset
								
								// 1. check for FMM bid
								if(FMM_hmap.containsKey(price)) {
									local_bidamount += FMM_hmap.get(price).get(0);
									local_UPbids += FMM_hmap.get(price).get(0);
								}
										
								// check whether market can be cleared
								if(local_bidamount>=split_FMM_tendAmount) {
									local_UP = price;
									local_surplus = local_bidamount - split_FMM_tendAmount;
									break;
								}
							}
							
							if(local_UP==-1) {
								for (int i: config) {
								  System.out.print(i);
								  System.out.print(" ");
								}
								System.out.println();
								Global.handleError("Error calculating uniform price: The tender amount was not matched for this config (see above) of node "+node);
							}
							
							/** 3. Calculate thisAN's utility */

							// Calculate utility for all time slots in one
							if(thisAN.bid_prices[0]<=local_UP) {
								
								double accAmount = thisAN.bid_amounts[0];
								
								if((thisAN.bid_prices[0]==local_UP)&&(local_surplus>0)){
									// In case of price equality, bids are accepted randomly
									// Expected utility for random acceptance is equal to expected utility for pro rata acceptance
									accAmount = (local_UPbids-local_surplus)*thisAN.bid_amounts[0]/local_UPbids; //prorataAmount = openTender*thisBid/allBids;
								}
								
								u = num_tSlots*getUtilityperSlot(num_tSlots,accAmount,(double)local_UP,thisAN.MW_cost[0],thisAN.MWh_cost[0],pCall); // getUtility returns utility per timeslot, multiply with number of timeslots()
							} // else: this node's bid is not accepted -> u=0.0
							break;
						default:
							Global.handleError("Error calculating payoffs: Unknown kind of playerclass " + thisAN.pcKind + " for ActionNode with index "+node);
						}

					} else {
						Global.handleError("Error determining correct choice of Nature.");
					}

				}
				u = Global.round(ROUND_DIGITS,u);
				payoffMap.put(IntBuffer.wrap((int[])config.clone()), new Double(u));
			}
		}
	}
	
	
	@Override
	protected String getGameHelp() {
		return "Generate a FMM_RLM bayesian action-graph game.\n";
	}

}
