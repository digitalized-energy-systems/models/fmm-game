package edu.stanford.multiagent.gamer;

import java.io.File;
import java.util.*;

/**
 * Class for parsing command line parameters
 * Keeps track of bookkeeping as well
 */

public class ParamParser
{
    
    public static ParamParser emptyParser = new ParamParser();

    // ------------------------------------------

    // Delimiter to split at space characters and to split at but keep square bracktes
    static public final String THIS_DELIMITER = " |(?<=\\])|(?=\\])|(?<=])|(?=])";

    // -- Data to hold semi-parsed arguments
    private HashMap argmap;
    private String description = null;

    private static class ParamPair 
    {
		Vector val;
		boolean used=false;
	
		public ParamPair(Vector val)
		{
		    this.val=val;
		    this.used=false;
		}
    }
    
    // added for FMM-Games
    private static class ScannerSet
    {
    	Vector val;
    	Scanner scanner;
    	String line;
    	String[] args;
    	Integer index;
    	
    	public ScannerSet(Vector val, Scanner scanner, String line, String[] args, Integer index)
    	{
    		this.val = val;
        	this.scanner=scanner;
        	this.line = line;
        	this.args = args;
        	this.index = index;
    	}
    }

    private int usedCount=0;

    /**
     * Constructor 
     */
    public ParamParser()
    {
		argmap=new HashMap();
		usedCount=0;
    }

    /**
     * ADDED FOR FMM-Games
     * Constructor to use if the parameters are coming from a nested 
     * hashmap and the hashmap is thus already set up.
     */
    public ParamParser(HashMap parmap)
    {
    	argmap=new HashMap();
    	argmap.putAll(parmap);
    	usedCount = 0;
    }
    
    /**
     * Constructor to use if the arguments are coming from the 
     * command line and are thus already parsed into an array.
     * Fills in the argmap.
     */
    public ParamParser(String[] args) throws Exception
    {
	argmap=new HashMap();
	usedCount=0;
	
	if (args == null) {
	  description = "[]";
	    return;
	}

	description = "[ " + GameOutput.arrayToString(args, " ") + " ]";

	int i=0;
	while(i<args.length)
	    {
		if(args[i].charAt(0)!='-')
		    throw new Exception("Error Parsing Parameters: " + args[i]);
		
		String key=args[i].substring(1); //param without "-"
		
		Vector val=new Vector();
		while((++i < args.length))
		{
		    // -- Case 1: doesn't start with -
		    // -- So it's either a normal value, or start of "parser" value
		    if(args[i].charAt(0)!='-')
			{
			    // -- If it's not a parser
			    if(args[i].charAt(0)!='[')
				{
				    val.add(args[i]);
				    continue;
				}

			    //-- else deal with parser param.
			    Vector cmdline=new Vector();
			    
			    //-first argument
			    if(args[i].length()!=1) // -- if no space after [
				{
				//cmdline.add(args[i].substring(1));
				    args[i] = args[i].substring(1);
				    i--;
				}

			    // -- add things till last closing ]
			    int openCount=1;
			    while( (openCount > 0) && (++i < args.length) )
				{
				    if(args[i].charAt(0)=='[')
					openCount++;

				    int clcc=closeCount(args[i]);
				    if(clcc>0)
					{
					    openCount-=clcc;
					    if(args[i].length()!=1)
						cmdline.add(args[i].substring(0,args[i].length()-1));
					}
				    else
					cmdline.add(args[i]);
				}
			    if(openCount>0)
				throw new Exception("Unbalanced []!\n"+description);

			    val.add(cmdline);
			    continue;
			}
		 
		    // -- Case 2: start's with -
		    // -- so it's either next param, or a negative number

		    if(args[i].length()==1)
			throw new Exception("Bad Parameter: -");

		    if(Character.isDigit(args[i].charAt(1)))
			{
			    val.add(args[i]);
			    continue;
			}

		    break; //it start's with -, not a number, so next param
		}				
		
        argmap.put(key,  new ParamPair(val)); // e.g. key = "players", val = [3]
	    }
    }
    
    /** ADDED FOR FMM-Games
     * Constructor to use if the arguments are coming from a 
     * config file. Fills in the argmap.
     */
    public ParamParser(File file) throws Exception
    {
		argmap=new HashMap();
		usedCount=0;
		
		if (file == null) {
		  description = "[]";
		    return;
		}
	
		description = " "; //"[ " + GameOutput.arrayToString(file, " ") + " ]";
		
	
		// Read in text file line by line using Scanner
		Scanner scanner = new Scanner(file);
		while(scanner.hasNextLine())
		{			
			//process each line
			String line = scanner.nextLine();
		    
		    // strip comment
		    if (line.indexOf('#')==0) continue;
		    else if(line.indexOf('#')>0) line=line.substring(0,line.indexOf('#'));

		    String[] args = line.split(THIS_DELIMITER);
		    int i = -1;
		    
		    while ((++i < args.length) || scanner.hasNextLine())
		    {
		    	// -- Case 0: reached end of line
				// -- So get next line first
			    if(i>=args.length) {
			    	
			    	// get next line
				    line = scanner.nextLine();
				    
				    // strip comment
				    if (line.indexOf('#')==0) continue;
				    else if(line.indexOf('#')>0) line=line.substring(0,line.indexOf('#'));

				    i=-1; // reset
				    args = line.split(THIS_DELIMITER);
				    continue;
			    }
		    	
			  //in case of empty string
		    	if(args[i].length()==0) continue; 
		    	
		    	// get param option
			    if(args[i].charAt(0)!='-')
				    throw new Exception("Error Parsing Parameters: " + args[i]);
				String key=args[i].substring(1); // option without "-"
			    
				// get param argument
				Vector val=new Vector();
				while((++i < args.length) || scanner.hasNextLine())
				{
					// -- Case 0: reached end of line
					// -- So get next line first
				    if(i>=args.length) {
				    	
				    	// get next line
					    line = scanner.nextLine();
					    
					    // strip comment
					    if (line.indexOf('#')==0) continue;
					    else if(line.indexOf('#')>0) line=line.substring(0,line.indexOf('#'));
	
					    i=-1; // reset
					    args = line.split(THIS_DELIMITER);
					    continue;
				    }
				    
				    //in case of empty string
				    if(args[i].length()==0) continue; 
					
					// -- Case 1: doesn't start with -
				    // -- So it's either a normal value, or start of "parser" value
				    if(args[i].charAt(0)!='-')
					{
					    // -- If it's not a parser
					    if(args[i].charAt(0)!='[')
						{
						    val.add(args[i]);
						    continue;
						}
		
					    //-- else deal with parser param
					    ScannerSet Variables = paramParseBrackets(scanner, line, args, i);
					    // Update variables
					    Vector brackets_vec = Variables.val;
					    val.add(brackets_vec);
					    scanner = Variables.scanner;
					    line = Variables.line;
					    args = Variables.args;
					    i = Variables.index;
					    continue;
					    
					} // end of "it doesn't start with '-' "
				    
				    // -- Case 2: start's with -
				    // -- so it's either next param, or a negative number
				    if(args[i].length()==1)
				    	throw new Exception("Bad Parameter: -");
		
				    if(Character.isDigit(args[i].charAt(1)))
					{
					    val.add(args[i]);
					    continue;
					}
				    
				    --i;
				    break; //it start's with -, not a number, so next param
				}
				
				argmap.put(key,  new ParamPair(val)); // e.g. key = "num_tSlots", val = [2]
				
		    }
		}
		scanner.close();	
	}
    /**
     * Method is called whenever ParamParser encounters opening square bracket '['
     * @param scanner file scanner
     * @param line current line of scanner
     * @param args parsed array if arguments in current line of file
     * @param i args index 
     * @param clcc closed count ']'
     * @return
     * @throws Exception 
     */
    private static ScannerSet paramParseBrackets(Scanner scanner, String line, String[] args, int i) throws Exception {
    	
    	Vector brackets_vec=new Vector(); 
    	HashMap brackmap = new HashMap(); // for inner parameter -option argument pairs
	    //int openCount = 1;
	    
	    //-first argument
	    if(args[i].length()!=1) // -- if no space after [
		{
		//cmdline.add(args[i].substring(1));
		    args[i] = args[i].substring(1);
		    i--;
		}

	    while ((++i < args.length) || scanner.hasNextLine())
	    {
	    	// -- Case 0: reached end of line
			// -- So get next line first
		    if(i>=args.length) {
		    	
		    	// get next line
			    line = scanner.nextLine();
			    
			    // strip comment
			    if (line.indexOf('#')==0) continue;
			    else if(line.indexOf('#')>0) line=line.substring(0,line.indexOf('#'));

			    i=-1; // reset
			    args = line.split(THIS_DELIMITER);
			    continue;
		    }
	    	
		    //in case of empty string
	    	if(args[i].length()==0) continue; 
	    	
	    	//-- Case 1: It's another opening square bracket '['
	    	//-- So recursively parse parameters
	    	if(args[i].charAt(0)=='[')
			{
	    		ScannerSet Variables = paramParseBrackets(scanner, line, args, i);
			    // Update variables
			    Vector innerbrackets_vec = Variables.val;
			    brackets_vec.add(innerbrackets_vec);
			    scanner = Variables.scanner;
			    line = Variables.line;
			    args = Variables.args;
			    i = Variables.index; 
			    continue;
			}
	    	
		    //-- Case 2: It's a closing square bracket ']'
	    	//-- So recursively parse parameters
	    	if(args[i].charAt(0)==']')
			{
	    		if(!brackmap.isEmpty())
	    			brackets_vec.add(brackmap); // add hashmap with inner parameters to vector
	    		
	    		return new ScannerSet(brackets_vec, scanner, line, args, i); 
			}
		    
	    	//-- Case 3: start's with -
		    //-- so it's either an inner next param, or a negative number
	    	if(args[i].charAt(0)=='-') {
			    if(args[i].length()==1)
			    	throw new Exception("Bad Parameter: -");
	
			    if(Character.isDigit(args[i].charAt(1)))
				{
			    	brackets_vec.add(args[i]);
				    continue;
				}
			    
			    //it start's with -, not a number, so inner next param
			    String key=args[i].substring(1); // option without "-"
			    
			    // get param argument
				Vector val=new Vector();
				while((++i < args.length) || scanner.hasNextLine())
				{
					// -- Case 0: reached end of line
					// -- So get next line first
				    if(i>=args.length) {
				    	
				    	// get next line
					    line = scanner.nextLine();
					    
					    // strip comment
					    if (line.indexOf('#')==0) continue;
					    else if(line.indexOf('#')>0) line=line.substring(0,line.indexOf('#'));

					    i=-1; // reset
					    args = line.split(THIS_DELIMITER);
					    continue;
				    }
				    
				    //in case of empty string
				    if(args[i].length()==0) continue; 
					
					// -- Case 1: doesn't start with -
				    // -- So it's either a normal value, or start of "parser" value
				    if(args[i].charAt(0)!='-')
					{
				    	//-- Case 1a: It's another opening square bracket '['
				    	//-- So recursively parse parameters
				    	if(args[i].charAt(0)=='[') {
				    		ScannerSet Variables = paramParseBrackets(scanner, line, args, i);
						    // Update variables
						    Vector innerbrackets_vec = Variables.val;
						    if(val.isEmpty())
						    	val = (Vector) innerbrackets_vec.clone();
						    else
						    	val.add(innerbrackets_vec);
						    scanner = Variables.scanner;
						    line = Variables.line;
						    args = Variables.args;
						    i = Variables.index; 
						    continue;
				    	}
				    	
				    	//-- Case 1b: It's a closing square bracket ']'
				    	//-- So recursively parse parameters
				    	if(args[i].charAt(0)==']') {
				    		--i;
						    break; // break while to return to calling method
				    	}
				    	
					    // -- Else Case 1c: It's not a parser, normal value
					    val.add(args[i]);
					    continue;
					    
					} // end of "it doesn't start with '-' "
				    
				    // -- Case 2: start's with -
				    // -- so it's either next param, or a negative number
				    if(args[i].length()==1)
				    	throw new Exception("Bad Parameter: -");
		
				    if(Character.isDigit(args[i].charAt(1)))
					{
					    val.add(args[i]);
					    continue;
					}
				    
				    --i;
				    break; //it start's with -, not a number, so next param
				}
				brackmap.put(key,  new ParamPair(val)); // e.g. key = "num_players", val = [2]
				continue;
	    	}
	    	
	    	// -- Case 4: normal value
	    	brackets_vec.add(args[i]);
	    }
	    throw new Exception("Unbalanced []!\n" + line);
    }
    
    /**
     * ADDED FOR FMM-GAMES
     * Merges two ParamParser objects 
     */
    public void merge(ParamParser other) throws Exception {
    	
    	// Check for duplicate keys in argmaps
    	Set<String> keySet = other.argmap.keySet();
        for(String key : keySet)
        {
        	if(this.argmap.containsKey(key))
        	{
        		ParamPair this_p = (ParamPair)this.argmap.get(key);
        		ParamPair other_p = (ParamPair)other.argmap.get(key);
        		
        		if(other_p.used)
        			throw new Exception("Parse error: Key: " + key + " has been defined twice as "
        					+ this_p.val + " and " + other_p.val + "!");
        		
        		// other_p.val has not yet been used, so it's not a major problem
        		System.err.println("WARNING: Key: "+key+" has been defined twice. " + this_p.val +
        				" takes effect but not " + other_p.val + ".");
        	}
        }
        
        // Merge ParamParsers
		other.argmap.putAll(this.argmap); 	// in case of duplicate keys, other.argmap entries  
        this.argmap = other.argmap; 		// will be overwritten by this.argmap
    	this.description = this.description.concat(other.description);
    	this.usedCount = this.usedCount + other.usedCount;
    } 

    /**
     * Checks number of closing braces ] in a string.
     */
    private static int closeCount(String s)
    {
	int count=0;
	int i=s.length();
	while( (--i>=0) && s.charAt(i)==']')
	    count++;

	return count;
    }

    /**
     * Checks if all arguments are used
     */
    public boolean hasUnusedArgs()
    {
	return (argmap.size() > usedCount);
    }

    /**
     * Returns unused arguments
     */
    public String[] getUnusedArgs()
    {
	String[] args=new String[argmap.size()-usedCount];
	
	Iterator it=argmap.entrySet().iterator();
	String key="";
	int i=0;
	while(it.hasNext())
	    {
		Map.Entry e = (Map.Entry)it.next();
		ParamPair p = (ParamPair)e.getValue();
		if(!p.used)
		    args[i++]=(String)e.getKey();
	    }

	return args;
    }


    /**
     * Fills in hashmap with parameter values
     */
    public void setParameters(Parameters pars) 
	throws Exception
    {
	Parameters.ParamInfo[] info = pars.getParamInfo(); //, HashMap params, boolean[] used

	for(int i=0; i<info.length; i++)
	    {
	      if(pars.isParamSet(i) && !argmap.containsKey(info[i].name))
		continue;

	      // -- Don't override previously set params
	      if(pars.setByUser(i))
		continue;

		boolean used = argmap.containsKey(info[i].name);
		
		if(used)
		    {
		      ParamPair p = (ParamPair)argmap.get(info[i].name);
                      if(!p.used)
			  {
			      p.used=true;
			      argmap.put(info[i].name, p);
			      usedCount++;
			  }
		      Object val=parseParam(p.val, info[i]);
		      pars.setParameter(i, val, true);
		    }
		else if (info[i].defaultValue!=null)
		    pars.setParameter(i, info[i].defaultValue, false);
	    }

    }

    /**
     * ADDED FOR FMM-GAMES
     * Fills in hashmap with parameter values
     */
    public void setParameters(Parameters pars, boolean bool) 
	throws Exception
    {
	Parameters.ParamInfo[] info = pars.getParamInfo(); //, HashMap params, boolean[] used
	
		for(int i=0; i<info.length; i++)
		{      
		  ParamPair p = (ParamPair)pars.getParameter(info[i].name);
	      Object val=parseParam(p.val, info[i]);
	      pars.setParameter(i, val, true);
	    }
    }
    
    /**
     * Convert String into an appropriate object
     */
    private Object parseParam(Vector val, Parameters.ParamInfo p)
	throws NumberFormatException, Exception
    {
	switch(p.type)
	    {
	    case Parameters.ParamInfo.LONG_PARAM:
		{
		    if(val.size()!=1)
			throw new Exception("Parse error: " + p.name + " takes a single argument!");
		    Long v = Long.valueOf((String)val.get(0));
		    long high=((Long)p.high).longValue();
		    long low=((Long)p.low).longValue();
		    if(v.longValue() < low || v.longValue() > high)
			throw new Exception("Out of range: " + p.name + "=" + v);
		    return v;
		}
		
	    case Parameters.ParamInfo.DOUBLE_PARAM:
		{
		    if(val.size()!=1)
			throw new Exception("Parse error: " + p.name + " takes a single argument!");
		    Double v = Double.valueOf((String)val.get(0));
		    double high=((Double)p.high).doubleValue();
		    double low=((Double)p.low).doubleValue();
		    if(v.doubleValue() < low || v.doubleValue() > high)
			throw new Exception("Out of range: " + p.name + "=" + v);
		    return v;
		}
		
	    case Parameters.ParamInfo.BOOLEAN_PARAM:
		{
		    if(val.size()> 1)
			throw new Exception("Parse error: " + p.name + " takes at most one argument!");
		    if(val.size()==0) // -- used as a flag, indicates true
			return Boolean.TRUE;
		    // -- otherwise need to parse the value
		    try {
			int v = Integer.valueOf((String)val.get(0)).intValue();
			if(v == 0)
			    return Boolean.FALSE;
			if(v == 1)
			    return Boolean.TRUE;
			
			throw new Exception();
		  } catch (Exception e) {
		      throw new Exception("Parse error: " + p.name + " takes 0 or 1 as an argument!");
		  }
		}
		
	    case Parameters.ParamInfo.STRING_PARAM:
		
		if(val.size()!=1)
		    throw new Exception("Parse error: " + p.name + " takes a single argument!");
		return val.get(0); 
		
	    case Parameters.ParamInfo.VECTOR_PARAM:
	      if(val.size()==0)
		throw new Exception("Parse error: " + p.name + " needs at least one argument!");
		return val;
		
	    case Parameters.ParamInfo.CMDLINE_PARAM:
		if(val.size()!=1)
		    throw new Exception("Parse error: " + p.name + " takes a single argument in []!");
		
	      Vector cmdline=(Vector)val.get(0);
	      ParamParser pVal = new ParamParser((String[])cmdline.toArray(new String[]{}));
	      return pVal;
	      
	    case Parameters.ParamInfo.NESTEDHASHMAP_PARAM: // ADDED FOR FMM-GAMES
			if(val.size()!=1)
			    throw new Exception("Parse error: " + p.name + " takes a single vector []!");
			
		      Vector topvec=(Vector)val.get(0);
		      if(topvec.size()==0)
		    	  throw new Exception("Parse error: " + p.name + " needs at least one argument in []!");
		      
		      return val;
	    
	    case Parameters.ParamInfo.HASHARRAY_PARAM: // ADDED FOR FMM-GAMES
	    case Parameters.ParamInfo.VECARRAY_PARAM:
	    	if(val.size()==0)
	    		throw new Exception("Parse error: " + p.name + " needs at least one argument!");
	    		return val;
		      
	    default:
	      throw new Exception("INTERNAL EXCEPTION: UNKNOWN PARAM TYPE!");
	    }
    }
    
    // -----------------------------------------------------

    /**
     * Converts to String
     */
    public String toString()
    {
      return description;
    }


    /**
     * Used for debugging only
     */
    public static void main(String[] args) throws Exception
    {
	ParamParser p=new ParamParser(args);
	System.out.println(p);
    }
}

