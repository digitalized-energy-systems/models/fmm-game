package edu.stanford.multiagent.gamer;

import java.util.*;
import java.nio.IntBuffer;

import edu.stanford.multiagent.gamer.functions.Function;
import edu.stanford.multiagent.gamer.functions.IncreasingTableFunction;
import edu.stanford.multiagent.gamer.graphs.ALGraph;
import edu.stanford.multiagent.gamer.graphs.Edge;

public class JobMarketGame extends ActionGraphGame {
	boolean randomize;
	
	private static final int subjects=3;

	private static final double LOWESTCOST = 1;
	private static final double HIGHESTCOST = 5;
	private static final double LOWESTVALUE = 3;
	private static final double HIGHESTVALUE = 15;
	
	private static int[] degrees = new int[subjects];
	
	//private static final int degrees=4;
	
	/* Parameters
	 *
	 */
	private static Parameters.ParamInfo[] jobParam;
	private static Parameters.ParamInfo weight_higher;
	private static Parameters.ParamInfo weight_lower;
	private static Parameters.ParamInfo weight_related;
	
	private static Parameters.ParamInfo valueFunction;
	private static Parameters.ParamInfo valueFunctionParams;

	private static Parameters.ParamInfo costFunction;
	private static Parameters.ParamInfo costFunctionParams;
	
//	double[] values= {2,4,6,8,10};
//	double[] costs= {1,2,3,4,5};

	
	static
	{
	    valueFunction=new Parameters.ParamInfo ("value_func",Parameters.ParamInfo.STRING_PARAM, null, null, "the name of the function class to use for education values ( should be monotnonically increasing)",false);
	    valueFunctionParams = new Parameters.ParamInfo("value_func_params", Parameters.ParamInfo.CMDLINE_PARAM, null, null, "parameters to be handed off to the far function, must be enclosed in [].",false);

	    costFunction=new Parameters.ParamInfo ("cost_func",Parameters.ParamInfo.STRING_PARAM, null, null, "the name of the function class to use for education costs ( should be monotonically increasing)",false);
	    costFunctionParams = new Parameters.ParamInfo("cost_func_params", Parameters.ParamInfo.CMDLINE_PARAM, null, null, "parameters to be handed off to the near function, must be enclosed in [].",false);

	    weight_higher=new Parameters.ParamInfo("higher", Parameters.ParamInfo.DOUBLE_PARAM, new Double(0.0), new Double(1), "weight higher. Must be >=0 and <=1",false);
	    weight_lower=new Parameters.ParamInfo("lower", Parameters.ParamInfo.DOUBLE_PARAM, new Double(0.0), new Double(1), "weight lower. Must be >=0 and <=1",false);
	    weight_related=new Parameters.ParamInfo("related", Parameters.ParamInfo.DOUBLE_PARAM, new Double(0.0), new Double(1), "weight related. Must be >=0 and <=1",false);

		jobParam= new Parameters.ParamInfo[] {Game.players, Game.actions, valueFunction, valueFunctionParams, costFunction, costFunctionParams, weight_higher, weight_lower, weight_related};
		
		Global.registerParams(JobMarketGame.class, jobParam );
	}
	
	public JobMarketGame() throws Exception
	{
		super();
	}


	protected String getGameHelp() {
		
		return "Job Market Game represented as an AGG";
	}


	public void checkParameters() throws Exception
	{

	}
	
	public void randomizeParameters() 
	  {
		try{
			int totalDegrees = Integer.valueOf(parameters.getVectorParameter(Game.symActions.name).get(0).toString()) -1;
			int numDegreeLevels = (int)Math.ceil( (double)totalDegrees/ subjects) + 1; 
		if(!parameters.setByUser(valueFunction.name)){
		    parameters.setParameter(valueFunction.name, 
				    "IncreasingTableFunction");
		    String[] params = { "-min", String.valueOf(LOWESTVALUE), "-max" , String.valueOf(HIGHESTVALUE), "-points", String.valueOf(numDegreeLevels)};
		    parameters.setParameter(valueFunctionParams.name,new ParamParser(params), true);
		}
		if(!parameters.setByUser(costFunction.name)){
			parameters.setParameter(costFunction.name, "IncreasingTableFunction");
		    String[] params = { "-min", String.valueOf(LOWESTCOST), "-max" , String.valueOf(HIGHESTCOST), "-points", String.valueOf(numDegreeLevels)};
		    parameters.setParameter(costFunctionParams.name,new ParamParser(params), true);
		}
//		if(!parameters.setByUser(weight_higher.name))
//			parameters.setParameter(weight_higher.name, (Global.rand.nextDouble()));
//		if(!parameters.setByUser(weight_lower.name))
//			parameters.setParameter(weight_lower.name, (Global.rand.nextDouble()));
//		if(!parameters.setByUser(weight_related.name))
//			parameters.setParameter(weight_related.name, (Global.rand.nextDouble()));

		} catch (Exception e) {
		    Global.handleError(e, "Randomizing Functions");
		}

		super.randomizeParameters();
	  }
	
	public void initialize() throws Exception
	{
		super.initialize();
		
		setNumPlayers((int)getLongParameter(Game.players.name));
		
		numActionNodes= Integer.valueOf( parameters.getVectorParameter(Game.actions.name).get(0).toString() ); // all same
		setNumActions((numActionNodes));
		numPNodes=0;
		
		// initialize each subject to have 0 degrees
		for( int subject = 0; subject < subjects; subject++ ){
			degrees[subject]  = 0;
		}
		
		int totalNumDegrees = numActionNodes - 1; // exclude highschool
		int createdDegrees = 0;
		// spread the degrees amongst subjects
		while( createdDegrees < totalNumDegrees ){
			degrees[createdDegrees % subjects ] += 1;
			createdDegrees ++ ;
		}
		
		//init actionSets
		actionSets = new int[getNumPlayers()][];
		for (int i=0;i<getNumPlayers();i++){
		  actionSets[i] = new int[getNumActions(i)];
		  for(int j=0;j<getNumActions(i);j++){
		    actionSets[i][j] = j;
		  }
		}
		
		initGraph();

	}
	protected void initGraph(){
		try
		{
			graph = new JobMarketGraph();
			graph.setParameter("numNodesParam",(long) numActionNodes,true);
			graph.initialize();
		}
		catch (Exception e)
		{
			Global.handleError(e, "initializing graph");
		}
	}
	
	private int getDegreeLevel( int node ){
		if ( node==0 )
			return 0;
		return ((node-1) / subjects) + 1;
	}
	
	public void setParameters(ParamParser p, boolean randomize)
			throws Exception {
		super.setParameters(p, randomize);
		this.randomize = randomize;
	}
	
	
	public void doGenerate() {
		
		String valueName = parameters.getStringParameter(valueFunction.name);
		ParamParser valueParams = parameters.getParserParameter(valueFunctionParams.name);

		String costName = parameters.getStringParameter(costFunction.name);
		ParamParser costParams = parameters.getParserParameter(costFunctionParams.name);

//		double[] values= {2,4,6,8,10};
//		double[] costs= {1,2,3,4,5};
//		double weight_higher= 0.1;
//		double weight_lower= 0.1;
//		double weight_related = 0.1;
		
		Function value =(Function) Global.getObjectOrDie(valueName,
			      Global.FUNC);
		Function cost=(Function) Global.getObjectOrDie(costName,
			      Global.FUNC);

		try {
			//value.setDomain(0, getNumPlayers());
			value.setParameters(valueParams, false);
			value.initialize();
			value.doGenerate();
			//cost.setDomain(0, getNumPlayers());
			cost.setParameters(costParams, false);
			cost.initialize();
			cost.doGenerate();

		} catch (Exception e) {
			System.err.println(getHelp());
			System.err.println(value.getHelp());
			System.err.println(cost.getHelp());
			Global.handleError(e, "Error initializing functions");
		}

		setDescription("Job Market Game represented as AGG\n" + getDescription());
		setName("Job Market Game");
		graph.doGenerate();
		
		TreeSet [] configs = initConfigs();
		for (int node = 0; node < numActionNodes; node++)
		{
			TreeMap<IntBuffer, Double> payoffMap = new TreeMap<IntBuffer, Double>();
			graph.setNodeData(node, payoffMap);
			
			Iterator confIter = configs[node].iterator();
			while (confIter.hasNext()){
				IntBuffer tt = (IntBuffer)confIter.next();
				int[] key = (int[])tt.array();
				//util
				double u=0.0;
				//get the degree level

				//int deg = (node==0)?0:(node-1)%degrees+1;
				int deg = getDegreeLevel(node);
				u = value.eval(deg);
				double denom=0;
				Iterator edgeIter = graph.getEdges(node);
				
				for (int idx = 0; edgeIter.hasNext(); idx++){
					
					Edge e = (Edge)edgeIter.next();
					int nei = e.getDest();
					int deg_nei= getDegreeLevel(nei);//(nei==0)?0:(nei-1)%degrees+1;
					
					if (nei==node){ // self
						denom += key[idx];
					}
					else if (deg_nei==deg-1){ // lower degree
						denom+= getDoubleParameter(weight_lower.name) * key[idx];
					}
					else if (deg_nei==deg+1){ // higher degree
						denom+=getDoubleParameter(weight_higher.name) * key[idx];
					}
					else{ // related degree
						denom+=getDoubleParameter(weight_related.name) * key[idx];
					}
					
				}
				u /= denom;
				u -= cost.eval(deg); //costs[deg];
				
				payoffMap.put(IntBuffer.wrap((int[])key.clone()), new Double(u));
			}
		}
	}
	
	
	private static class JobMarketGraph extends ALGraph{
		
		
		protected static Parameters.ParamInfo[] jobgParam;
		protected static Parameters.ParamInfo _numNodes;
		
		static
		{
			_numNodes = new Parameters.ParamInfo("numNodesParam", Parameters.ParamInfo.LONG_PARAM, new Long(1), new Long(100), "Number of actions.  Must be > 0.  May be set up to 100.");
			jobgParam=new Parameters.ParamInfo[] {_numNodes};
			Global.registerParams(JobMarketGraph.class, jobgParam);
		}
		
		protected String getGraphHelp()
		{
			return "Action graph for the Job Market game.";
		}
		
		JobMarketGraph( ) throws Exception {
			super();
		}
		public void initialize() throws Exception
		{
			super.initialize();
			
			long numNodes = 0;
		    try {
				numNodes= (parameters.getLongParameter(_numNodes.name));
			    } 
		    catch (Exception e) {
				Global.handleError(e, "Unable to get parameters to " +
						   "initialize JobMarketGame");
			    }
			    
			if (nodes.isEmpty())
			{
				for (int i = 0; i < numNodes; i++) addNode();
			}
			
		}
		public boolean reflexEdgesOk()
		{
			return (true);
		}
		public boolean hasSymEdges()
		{
			return false;
		}
		private int getNodeIndex( int subject, int degree ){
			return degree*JobMarketGame.subjects + subject + 1;
		}
		
		
		public void doGenerate()
		{
			//node 0: high school
			//nodes [1+i*degrees, (i+1)*degrees]: degrees for subject i
			// new
			// nodes [
			long numNodes = 0;
		    try {
				numNodes= getLongParameter(_numNodes.name);
			    } 
		    catch (Exception e) {
				Global.handleError(e, "Unable to get parameters to " +
						   "initialize JobMarketGame");
			    }

			//self edges
			for (int i = 0; i < numNodes; i++) addEdge(i, i);
			
			//edges between subjects
			for( int subject = 1; subject < subjects; subject++ ){
				// between subs 0 and 1 , 1 and 2
				for( int degree = 0; degree < degrees[subject]; degree++ ){
					// make sure the other subject has this degree
					// this is guaranteed by the way we distribute the nodes
					// sub 0 would always have at least as many nodes as sub 1
					// sub 1 >= sub2 
					addEdge( getNodeIndex(subject, degree), getNodeIndex(subject-1, degree));
					addEdge( getNodeIndex(subject-1, degree), getNodeIndex(subject, degree));
				}
			}
			
			// add edge between deg 0 of each subject to highschool 
			for (int sub=0;sub<subjects;sub++){
			  addEdge(0, getNodeIndex(sub, 0));
			  addEdge(getNodeIndex(sub,0), 0);
			}
			
			// add edges between degree and degree-1 in the same subject
			for (int sub=0;sub<subjects;sub++){
			  for (int deg=1;deg<degrees[sub];deg++){
				  addEdge( getNodeIndex(sub,deg), getNodeIndex(sub, deg-1));
				  addEdge( getNodeIndex(sub,deg-1), getNodeIndex(sub,deg));
			  }
			}
		
		}
		
	}

}
