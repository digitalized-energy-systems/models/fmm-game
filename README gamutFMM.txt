AGG Generators in GAMUT
Albert Xin Jiang (jiang@cs.ubc.ca)
Based on the GAMUT project (http://gamut.stanford.edu/)

There are four generators that uses AGG: RandomSymmetricAGG, 
CoffeeShopGame, IceCreamGame, and JobMarketGame. Descriptions of these games can be found in the following paper:

A.X. Jiang, K. Leyton-Brown and N. Bhat, Action-Graph Games, Games and Economic Behavior, Volume 71, Issue 1, January 2011, Pages 141-173. 

GAMUT's default output format is in normal form. Use the option 
"-output SpecialOutput" for generating AGG description 
files instead. The file format is  specified in 
http://www.cs.ubc.ca/~jiang/aggsoft/AGGFORMAT.txt 

Example commands:

java -jar gamut.jar -g RandomSymmetricAGG -players 4 -actions 4 -random_params -output SpecialOutput

java -jar gamut.jar -g IceCreamGame -players 3 -actions 8 -random_params -output SpecialOutput

java -jar gamut.jar -g CoffeeShopGame -players 2 -rows 4 -columns 4 -home_func PolyFunction -home_func_params [ -degree 3 -coefs  0 0 0 -1   ] -near_func PolyFunction -near_func_params [  -degree 2 -coefs  0 0 -1   ] -far_func PolyFunction -far_func_params [ -degree 1 -coefs  5 -0.5  ] -combine_func SumFunction -combine_func_params [ ] -output SpecialOutput

java -jar gamut.jar -random_seed 1 -g JobMarketGame -players 15 -actions 7 -random_params -output SpecialOutput


### Extension for FMM Games (Gesa Ohlendorf, Paul Hendrik Tiemann): ###

Options can be defined both in cmd as well as in .txt file with -f_config "<path>". If defined twice, cmd option takes effect.

Example commands for AGGs:

java -jar gamutFMM.jar -g FMMGame -output SpecialOutput -f_config "<path>\FMM_Parserconfig.txt"

java -jar gamutFMM.jar -g FMMRLMGame -output SpecialOutput -f_config "<path>\FMMRLM_Parserconfig.txt"

Example command for BAGG:

java -jar gamutFMM.jar -g FMMRLMBayesGame -output SpecialOutput -f_config "<path>\FMMRLMBayes_Parserconfig.txt"

java -jar gamutFMM.jar -g FMMRLMBayesSplitGame -output SpecialOutput -f_config "<path>\FMMRLMBayesSplit_Parserconfig.txt"